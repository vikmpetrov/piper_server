#!/bin/bash
echo Setting up Piper Server, please wait...
export PIPER_SERVER_ROOT=$PWD
export PIPER_OS=linux
cd ./resources/linux
source setup_python.sh
source setup_apache.sh
source setup_shotgun_api3.sh
cd $PIPER_SERVER_ROOT
echo Setting up virtual environment...
$PIPER_SERVER_ROOT/resources/linux/Python37/bin/python3 -m pip install virtualenv
$PIPER_SERVER_ROOT/resources/linux/Python37/bin/python3 -m virtualenv venv -p $PIPER_SERVER_ROOT/resources/linux/Python37/bin/python3
source venv/bin/activate
pip install -r requirements.txt
echo Piper Server setup completed.