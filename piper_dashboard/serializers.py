# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from piper_core.models import *
from rest_framework import serializers


BASE_FIELDS = ['type', 'id', 'name', 'long_name', 'description', 'created_by', 'created', 'updated_by', 'updated',
               'locked', 'metadata', 'full_url', 'dependency', 'display_fields']


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    fields_filter = set(['type', 'id', 'name', 'long_name'])
    request_data = None

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)
        if fields:
            if '__all__' in fields:
                self.fields_filter = []
            else:
                self.fields_filter = set(self.fields_filter).union(set(fields))

        # Instantiate the superclass normally:
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if self.fields_filter:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = self.fields_filter
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

    def save(self, request_data=None):
        self.request_data = request_data
        return super(DynamicFieldsModelSerializer, self).save()

    def update(self, instance, validated_data):
        for key in validated_data:
            setattr(instance, key, self.request_data[key])
        instance.save()
        return instance


class DependencySerializer(DynamicFieldsModelSerializer):
    type = 'Dependency'

    class Meta:
        model = Dependency
        fields = ['type', 'id', 'name', 'dependant_model', 'dependant_id', 'dependencies',
                  'created_by']


class StatusSerializer(DynamicFieldsModelSerializer):

    class Meta:
        model = Status
        fields = BASE_FIELDS


class ProjectSerializer(DynamicFieldsModelSerializer):
    status = StatusSerializer()

    class Meta:
        model = Project
        fields = BASE_FIELDS + ['episodic', 'status', 'publish_path_template', 'work_path_template',
                                'file_name_template']


class DepartmentSerializer(DynamicFieldsModelSerializer):
    status = StatusSerializer()

    class Meta:
        model = Department
        fields = BASE_FIELDS + ['email', 'status']


class EpisodeSerializer(DynamicFieldsModelSerializer):
    status = StatusSerializer()
    project = ProjectSerializer()

    class Meta:
        model = Episode
        fields = BASE_FIELDS + ['project', 'status']


class SequenceShotSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()

    class Meta:
        model = Shot
        fields = BASE_FIELDS + ['project', 'episode', 'status', 'duration', 'pre_roll', 'post_roll']


class SequenceSerializer(DynamicFieldsModelSerializer):
    status = StatusSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()
    shots = SequenceShotSerializer(many=True)

    class Meta:
        model = Sequence
        fields = BASE_FIELDS + ['project', 'episode', 'status', 'shots']


class AssetTypeSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = AssetType
        fields = BASE_FIELDS


class CategorySerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = Category
        fields = BASE_FIELDS


class AssetSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()
    asset_type = AssetTypeSerializer()

    class Meta:
        model = Asset
        fields = BASE_FIELDS + ['asset_type', 'project', 'episode', 'status', 'sequence']


class ShotSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()
    sequence = SequenceSerializer()

    class Meta:
        model = Shot
        fields = BASE_FIELDS + ['project', 'episode', 'sequence', 'status', 'duration', 'pre_roll', 'post_roll']


class StepSerializer(DynamicFieldsModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Step
        fields = BASE_FIELDS + ['category', 'level']


class UserTaskSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    step = StepSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()
    asset = AssetSerializer(fields=BASE_FIELDS + ['sequence'])
    shot = ShotSerializer(fields=BASE_FIELDS + ['sequence'])

    class Meta:
        model = Task
        fields = BASE_FIELDS + ['step', 'project', 'episode', 'asset', 'shot',
                                'start_date', 'end_date', 'status', 'is_active']


class UserSerializer(DynamicFieldsModelSerializer):
    status = StatusSerializer()
    department = DepartmentSerializer()
    projects = ProjectSerializer(many=True)
    tasks = UserTaskSerializer(many=True)

    class Meta:
        model = User
        fields = BASE_FIELDS + ['department', 'projects', 'is_human', 'is_superuser', 'status', 'tasks',
                                'thumbnail_url', 'username']


class TaskResourceSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    artist = UserSerializer()
    category = CategorySerializer()

    class Meta:
        model = Resource
        fields = BASE_FIELDS + ['source', 'artist', 'status', 'category']


class TaskSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    step = StepSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()
    asset = AssetSerializer()
    shot = ShotSerializer()
    artists = UserSerializer(many=True)
    resources = TaskResourceSerializer(many=True)

    class Meta:
        model = Task
        fields = BASE_FIELDS + ['step', 'project', 'episode', 'asset', 'shot', 'artists', 'resources',
                                'start_date', 'end_date', 'status', 'is_active', 'artists_set', 'artists_list']


class ResourceSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    artist = UserSerializer()
    task = TaskSerializer()
    category = CategorySerializer()

    class Meta:
        model = Resource
        fields = BASE_FIELDS + ['source', 'task', 'artist', 'status', 'category']


class VersionFileSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    artist = UserSerializer()

    class Meta:
        model = File
        fields = BASE_FIELDS + ['format', 'is_sequence', 'is_work_file', 'artist', 'status']


class VersionSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    artist = UserSerializer()
    resource = ResourceSerializer()
    work_file = VersionFileSerializer()

    class Meta:
        model = Version
        fields = BASE_FIELDS + ['resource', 'artist', 'status', 'work_path', 'work_file', 'for_dailies']


class FileSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    status = StatusSerializer()
    artist = UserSerializer()
    version = VersionSerializer()
    resource = ResourceSerializer()
    task = TaskSerializer()
    shot = ShotSerializer()
    sequence = SequenceSerializer()
    asset = AssetSerializer()
    project = ProjectSerializer()

    class Meta:
        model = File
        fields = BASE_FIELDS + ['format', 'is_sequence', 'is_work_file', 'version', 'resource', 'task', 'shot',
                                'sequence', 'asset', 'project', 'artist', 'status']


class ConfigSerializer(DynamicFieldsModelSerializer):
    status = StatusSerializer()

    class Meta:
        model = Config
        fields = BASE_FIELDS + ['config_rules', 'status', 'inherited_config_rules']


class WorkLogSerializer(DynamicFieldsModelSerializer):
    task = TaskSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()
    artist = UserSerializer()

    class Meta:
        model = WorkLog
        fields = BASE_FIELDS + ['project', 'episode', 'task', 'artist', 'duration']


class SystemSerializer(DynamicFieldsModelSerializer):
    type = 'System'

    class Meta:
        model = System
        fields = ['type', 'id', 'name', 'long_name', 'platform', 'publish_root', 'work_root', 'software_root',
                  'modules_root']


class SoftwareSerializer(DynamicFieldsModelSerializer):
    category = CategorySerializer()
    system = SystemSerializer()

    class Meta:
        model = Software
        fields = BASE_FIELDS + ['category', 'system', 'executable', 'file_extensions', 'module_environment_variable',
                                'render_formats', 'is_farm', 'command_line_renderer']


class ModuleSerializer(DynamicFieldsModelSerializer):
    software = SoftwareSerializer(many=True)

    class Meta:
        model = Module
        fields = BASE_FIELDS + ['software', 'root']


class ProfileSerializer(DynamicFieldsModelSerializer):
    software = SoftwareSerializer(many=True)
    modules = ModuleSerializer(many=True)

    class Meta:
        model = Profile
        fields = BASE_FIELDS + ['software', 'modules']


class PlaylistSerializer(DynamicFieldsModelSerializer):
    project = ProjectSerializer()
    episode = EpisodeSerializer()

    class Meta:
        model = Playlist
        fields = BASE_FIELDS + ['project', 'episode', 'version_ids']


class TagSerializer(DynamicFieldsModelSerializer):
    type = 'Tag'

    class Meta:
        model = Tag
        fields = ['type', 'id', 'name', 'link_id', 'metadata', 'created', 'created_by']


class NoteSerializer(DynamicFieldsModelSerializer):
    type = 'Note'
    project = ProjectSerializer()
    author = UserSerializer()
    mentioned_users = UserSerializer(many=True)
    tags = TagSerializer(many=True)

    class Meta:
        model = Note
        fields = ['project', 'type', 'id', 'content', 'created', 'author', 'mentioned_users', 'tags', 'created_by']


class TaskTemplateSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = TaskTemplate
        fields = BASE_FIELDS + ['tasks']


class EditTypeSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = EditType
        fields = BASE_FIELDS


class EditSerializer(DynamicFieldsModelSerializer):
    dependency = DependencySerializer()
    edit_type = EditTypeSerializer()
    project = ProjectSerializer()
    episode = EpisodeSerializer()

    class Meta:
        model = Edit
        fields = BASE_FIELDS + ['edit_type', 'duration', 'frames', 'project', 'episode',
                                'timecode_start', 'timecode_end', 'fps', 'edl', 'items']


class AttachmentSerializer(DynamicFieldsModelSerializer):
    type = 'Attachment'
    file = FileSerializer()
    author = UserSerializer()

    class Meta:
        model = Attachment
        fields = ['type', 'id', 'file', 'author', 'created', 'created_by']


class GlobalsConfigSerializer(DynamicFieldsModelSerializer):
    type = 'GlobalsConfig'

    class Meta:
        model = GlobalsConfig
        fields = ['type', 'id', 'name', 'start_frame', 'email_on_update', 'email_host', 'email_port', 'email_username',
                  'email_password', 'ticket_email', 'shotgun_sync', 'shotgun_url', 'shotgun_script_name',
                  'shotgun_application_key', 'can_sg_sync', 'email_on_publish', 'email_ready', 'auto_update_thumbnails']


class HistorySerializer(DynamicFieldsModelSerializer):
    type = 'History'

    class Meta:
        model = History
        fields = ['type', 'id', 'before', 'after', 'created', 'created_by']


MODEL_SERIALIZER = {
    'Status': StatusSerializer,
    'User': UserSerializer,
    'Department': DepartmentSerializer,
    'Tag': TagSerializer,
    'Note': NoteSerializer,
    'Project': ProjectSerializer,
    'AssetType': AssetTypeSerializer,
    'Category': CategorySerializer,
    'Asset': AssetSerializer,
    'Episode': EpisodeSerializer,
    'Sequence': SequenceSerializer,
    'Shot': ShotSerializer,
    'Step': StepSerializer,
    'TaskTemplate': TaskTemplateSerializer,
    'Task': TaskSerializer,
    'Resource': ResourceSerializer,
    'Version': VersionSerializer,
    'Dependency': DependencySerializer,
    'File': FileSerializer,
    'Attachment': AttachmentSerializer,
    'EditType': EditTypeSerializer,
    'Edit': EditSerializer,
    'WorkLog': WorkLogSerializer,
    'System': SystemSerializer,
    'Software': SoftwareSerializer,
    'Profile': ProfileSerializer,
    'Module': ModuleSerializer,
    'Playlist': PlaylistSerializer,
    'GlobalsConfig': GlobalsConfigSerializer,
    'Config': ConfigSerializer,
    'History': HistorySerializer
}
