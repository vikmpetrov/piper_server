# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from django import forms
from django.contrib.postgres.forms import SimpleArrayField
from django.db.models import Q
from datetime import datetime
from piper_core.models import *


class GlobalsConfigForm(forms.ModelForm):

    class Meta:
        model = GlobalsConfig
        fields = ['studio_name', 'start_frame', 'auto_update_thumbnails', 'email_on_publish', 'email_on_update',
                  'email_host', 'email_port', 'email_username', 'email_password', 'ticket_email',
                  'shotgun_sync', 'shotgun_url', 'shotgun_script_name', 'shotgun_application_key']
        widgets = {
                    'studio_name': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Studio name.'}),
                    'start_frame': forms.NumberInput(attrs={'class': 'form-control',
                                                           'title': 'Default project start frame.'}),
                    'auto_update_thumbnails': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                                   'title': 'Toggle automatically updated thumbnails.'
                                                                   }),
                    'email_on_publish': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                           'title': 'Toggle e-mail notifications on publish by default.'
                                                            }),
                    'email_on_update': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                           'title': 'Toggle e-mail notifications on updates by default.'
                                                            }),
                    'email_host': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Studio e-mail host server.'}),
                    'email_port': forms.NumberInput(attrs={'class': 'form-control',
                                                           'title': 'Studio e-mail host server port.'}),
                    'email_username': forms.TextInput(attrs={'class': 'form-control',
                                                      'title': 'No-reply notification e-mail account username.'}),
                    'email_password': forms.PasswordInput(attrs={'class': 'form-control',
                                                          'title': 'No-reply notification e-mail account password.'},
                                                          render_value=True),
                    'ticket_email': forms.TextInput(attrs={'class': 'form-control',
                                                           'title': 'Default e-mail address for ticket submission.'}),
                    'shotgun_sync': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                               'title': 'Toggle automatic sync to Shotgun.'
                                                               }),
                    'shotgun_url': forms.TextInput(attrs={'class': 'form-control',
                                                          'title': 'Shotgun server URL.'}),
                    'shotgun_script_name': forms.TextInput(attrs={'class': 'form-control',
                                                          'title': 'Shotgun script name.'}),
                    'shotgun_application_key': forms.TextInput(attrs={'class': 'form-control',
                                                          'title': 'Shotgun application key.'})
                   }


class SystemForm(forms.ModelForm):

    class Meta:
        model = System
        fields = ['publish_root', 'work_root', 'software_root', 'modules_root']
        widgets = {'publish_root': forms.TextInput(attrs={'class': 'form-control',
                                                          'title': 'Published files root directory.'}),
                   'work_root': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Work files root directory.'}),
                   'software_root': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Software root directory.'}),
                   'modules_root': forms.TextInput(attrs={'class': 'form-control',
                                                          'title': 'Modules root directory.'})
                   }


class SoftwareForm(forms.ModelForm):
    system = forms.ModelChoiceField(queryset=System.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Software operating system.'}))
    category = forms.ModelChoiceField(queryset=Category.objects.all(), required=False,
                                      widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                 'title': 'Software category.'}))

    class Meta:
        model = Software
        fields = ['system', 'name', 'long_name', 'executable', 'category', 'file_extensions', 'render_formats',
                  'module_environment_variable', 'command_line_renderer', 'is_farm']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Software name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Software long name.'}),
                   'root': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Software root directory.'}),
                   'executable': forms.TextInput(attrs={'class': 'form-control',
                                                 'title': 'Software bin executable.'}),
                   'command_line_renderer': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Software command line render executable.'}),
                   'module_environment_variable': forms.TextInput(attrs={'class': 'form-control',
                                                                  'title': 'Software module environment variable.'})}


class ModuleForm(forms.ModelForm):
    software = forms.ModelMultipleChoiceField(queryset=Software.objects.all())

    class Meta:
        model = Module
        fields = ['name', 'root', 'software']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Module name.'}),
                   'root': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Module root.'})
                   }


class ProfileForm(forms.ModelForm):
    software = forms.ModelMultipleChoiceField(queryset=Software.objects.all())
    modules = forms.ModelMultipleChoiceField(queryset=Module.objects.all())

    class Meta:
        model = Profile
        fields = ['name', 'software', 'modules']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Profile name.'})
                   }


class DepartmentForm(forms.ModelForm):
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Department}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Department
        fields = ['name', 'long_name', 'status', 'email', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Project short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Project long name.'}),
                   'email': forms.TextInput(attrs={'class': 'form-control',
                                                   'title': 'User e-mail address.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Studio description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle project locked.'})
                   }


class UserForm(forms.ModelForm):
    department = forms.ModelChoiceField(queryset=Department.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                   'title': 'User department.'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'department', 'description', 'is_human',
                  'is_superuser')
        widgets = {'first_name': forms.TextInput(attrs={'class': 'form-control',
                                                        'title': 'User first name.'}),
                   'last_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'User last name.'}),
                   'username': forms.TextInput(attrs={'class': 'form-control',
                                                      'title': 'Login username.'}),
                   'email': forms.TextInput(attrs={'class': 'form-control',
                                                   'title': 'User e-mail address.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'User description.'}),
                   'is_human': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                          'title': 'Toggle user is human.'}),
                   'is_superuser': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                              'title': 'Toggle user has superuser permissions.'})
                   }


class UserProfileForm(forms.ModelForm):
    department = forms.ModelChoiceField(queryset=Department.objects.all(), widget=forms.HiddenInput())

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'password', 'email', 'department', 'description')
        widgets = {'first_name': forms.TextInput(attrs={'class': 'form-control',
                                                        'title': 'User first name.'}),
                   'last_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'User last name.'}),
                   'username': forms.TextInput(attrs={'class': 'form-control',
                                                      'title': 'Login username.'}),
                   'email': forms.TextInput(attrs={'class': 'form-control',
                                                   'title': 'User e-mail address.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'User description.'}),
                   'password': forms.PasswordInput(attrs={'class': 'form-control',
                                                          'title': 'User password.'})
                   }

    def save(self, commit=True):
        # Save provided password in hashed format:
        user = super(UserProfileForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        user.is_human = True
        if commit:
            user.save()
        return user


class UserCreationForm(forms.ModelForm):
    department = forms.ModelChoiceField(queryset=Department.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                   'title': 'User department.'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'password', 'email', 'department', 'description',
                  'is_human', 'is_superuser')

        widgets = {'first_name': forms.TextInput(attrs={'class': 'form-control',
                                                        'title': 'User first name.'}),
                   'last_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'User last name.'}),
                   'username': forms.TextInput(attrs={'class': 'form-control',
                                                      'title': 'Login username.'}),
                   'email': forms.TextInput(attrs={'class': 'form-control',
                                                   'title': 'User e-mail address.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'User description.'}),
                   'is_human': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                          'title': 'Toggle user is human.'}),
                   'is_superuser': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                              'title': 'Toggle user has superuser permissions.'}),
                   'password': forms.PasswordInput(attrs={'class': 'form-control',
                                                          'title': 'User password.'})
                   }

    def save(self, commit=True):
        # Save provided password in hashed format:
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class CustomLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput(render_value=False, attrs={'class': 'form-control',
                                                                                     'title': 'Login password.'}))


class AttachmentForm(forms.ModelForm):
    class Meta:
        model = Attachment
        fields = ('file', )
        widgets = {'file': forms.FileInput(attrs={"style": "display: none;", "multiple": ''})}


class WorkLogForm(forms.ModelForm):
    project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput())
    artist = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.HiddenInput())
    task = forms.ModelChoiceField(queryset=Task.objects.all(), widget=forms.HiddenInput())

    class Meta:
        model = WorkLog
        fields = ('duration', 'description', 'project', 'artist', 'task')


class StatusForm(forms.ModelForm):

    class Meta:
        model = Status
        fields = ['name', 'long_name', 'description', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Status short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Status long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Status description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle status locked.'})
                   }


class EditTypeForm(forms.ModelForm):

    class Meta:
        model = EditType
        fields = ['name', 'long_name', 'description', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Edit type short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Edit type long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Edit type description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle edit type locked.'})
                   }


class ProjectForm(forms.ModelForm):
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Project}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Project status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Project
        fields = ['name', 'long_name', 'episodic', 'status', 'description', 'publish_path_template',
                  'work_path_template', 'file_name_template', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Project short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Project long name.'}),
                   'episodic': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                          'title': 'Toggle project episodic.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Project description.'}),
                   'publish_path_template': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Project publish path template.'}),
                   'work_path_template': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Project work path template.'}),
                   'file_name_template': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Project file name template.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle project locked.'})
                   }


class EpisodeForm(forms.ModelForm):
    project_id = forms.IntegerField(widget=forms.HiddenInput())
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Episode}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))

    class Meta:
        model = Episode
        fields = ['project_id', 'name', 'long_name', 'status', 'duration', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Episode short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Episode long name.'}),
                   'duration': forms.NumberInput(attrs={'class': 'form-control',
                                                        'title': 'Episode duration in frames.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Episode description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle episode locked.'})
                   }


class SequenceForm(forms.ModelForm):
    project_id = forms.IntegerField(widget=forms.HiddenInput())
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Sequence}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))

    class Meta:
        model = Sequence
        fields = ['project_id', 'name', 'long_name', 'status', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Sequence short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Sequence long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Sequence description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle sequence locked.'})
                   }


class EpisodeSequenceForm(forms.ModelForm):
    episode_id = forms.IntegerField(widget=forms.HiddenInput())
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Sequence}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))

    class Meta:
        model = Sequence
        fields = ['episode_id', 'name', 'long_name', 'status', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Sequence short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Sequence long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Sequence description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle sequence locked.'})
                   }


class ShotForm(forms.ModelForm):
    sequence_id = forms.IntegerField(widget=forms.HiddenInput())
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Shot}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Shot
        fields = ['sequence_id', 'name', 'long_name', 'status', 'duration', 'pre_roll', 'post_roll', 'description',
                  'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Shot short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Shot long name.'}),
                   'duration': forms.NumberInput(attrs={'class': 'form-control',
                                                        'title': 'Shot duration in frames.'}),
                   'pre_roll': forms.NumberInput(attrs={'class': 'form-control',
                                                           'title': 'Shot pre-roll in frames.'}),
                   'post_roll': forms.NumberInput(attrs={'class': 'form-control',
                                                           'title': 'Shot post-roll in frames.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Shot description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle shot locked.'})
                   }


class AssetTypeForm(forms.ModelForm):
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{AssetType}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = AssetType
        fields = ['name', 'long_name', 'status', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Asset type short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Asset type long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Asset type description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle asset type locked.'})
                   }


class CategoryForm(forms.ModelForm):
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Category
        fields = ['name', 'long_name', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Category short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Category long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Category description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle category locked.'})
                   }


class StepForm(forms.ModelForm):
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Step}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    category = forms.ModelChoiceField(queryset=Category.objects.all(),
                                      widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                 'title': 'Step category.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Step
        fields = ['name', 'long_name', 'status', 'level', 'category', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Pipeline step short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Pipeline step long name.'}),
                   'level': forms.TextInput(attrs={'class': 'form-control',
                                                   'title': 'Pipeline step level, e.g. "shot", "asset", etc.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Pipeline step description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle pipeline step locked.'})
                   }


class EpisodeAssetForm(forms.ModelForm):
    episode_id = forms.IntegerField(widget=forms.HiddenInput())
    asset_type = forms.ModelChoiceField(queryset=AssetType.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                   'title': 'Asset type.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Asset}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Asset
        fields = ['episode_id', 'name', 'long_name', 'asset_type', 'status', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Asset short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Asset long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Asset description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle asset locked.'})
                   }


class AssetForm(forms.ModelForm):
    project_id = forms.IntegerField(widget=forms.HiddenInput())
    asset_type = forms.ModelChoiceField(queryset=AssetType.objects.all(),
                                        widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                   'title': 'Asset type.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Asset}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Asset
        fields = ['project_id', 'name', 'long_name', 'asset_type', 'status', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Asset short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Asset long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Asset description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle asset locked.'})
                   }


class EditForm(forms.ModelForm):
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Edit}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    sequence_id = forms.IntegerField(widget=forms.HiddenInput())
    edit_type = forms.ModelChoiceField(queryset=EditType.objects.all(),
                                       widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                  'title': 'Edit type.'}))
    duration = forms.TimeField()
    edl = forms.FileField()
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Edit
        fields = ['sequence_id', 'name', 'long_name', 'edit_type', 'episode', 'sequence', 'frames',
                  'status', 'duration', 'fps', 'timecode_start', 'timecode_end', 'items', 'description', 'tags',
                  'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Edit short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Edit long name.'}),
                   'timecode_start': forms.TextInput(attrs={'class': 'form-control',
                                                            'title': 'Edit description.'}),
                   'timecode_end': forms.TextInput(attrs={'class': 'form-control',
                                                          'title': 'Edit description.'}),
                   'frames': forms.NumberInput(attrs={'class': 'form-control',
                                                      'title': 'Number of edit frames.'}),
                   'fps': forms.NumberInput(attrs={'class': 'form-control',
                                                   'title': 'Edit frames per second.'}),
                   'items': forms.TextInput(attrs={'class': 'form-control',
                                                   'title': 'Edit items.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Edit description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle edit locked.'})
                   }


class TaskForm(forms.ModelForm):
    step = forms.ModelChoiceField(queryset=Step.objects.all(),
                                  widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                             'title': 'Task step.'}))
    project = forms.ModelChoiceField(queryset=Project.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                'title': 'Task project.'}))
    episode = forms.ModelChoiceField(queryset=Episode.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                                'title': 'Task episode.'}))
    asset = forms.ModelChoiceField(queryset=Asset.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Task asset if applicable.'}))
    shot = forms.ModelChoiceField(queryset=Shot.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Task shot if applicable.'}))
    artists = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False,
                                             widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                                'title': 'Task artists.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Task}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(), required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))

    start_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                     widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))
    end_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                   widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))

    class Meta:
        model = Task
        fields = ['name', 'long_name', 'status', 'project', 'episode', 'asset', 'shot', 'artists', 'step',
                  'start_date', 'end_date', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Task short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Task long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Task description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle task locked.'})
                   }


class TaskManagerForm(forms.ModelForm):
    step = forms.ModelChoiceField(queryset=Step.objects.all(),
                                  widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                             'title': 'Task step.'}))
    asset_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    shot_id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    _parent_model = forms.CharField(widget=forms.HiddenInput())
    _parent_id = forms.IntegerField(widget=forms.HiddenInput())
    artists = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False,
                                             widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                                'title': 'Task artists.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Task}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))
    start_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                     widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))
    end_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                   widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))

    class Meta:
        model = Task
        fields = ['name', 'long_name', 'status', 'asset_id', 'shot_id', 'artists', 'step',
                  'start_date', 'end_date', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Task short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Task long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Task description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle task locked.'})
                   }


class AssetTaskForm(forms.ModelForm):
    step = forms.ModelChoiceField(queryset=Step.objects.filter(level='Asset'),
                                  widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                             'title': 'Task step.'}))
    asset_id = forms.IntegerField(widget=forms.HiddenInput())
    artists = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False,
                                             widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                                'title': 'Task artists.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Task}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))
    start_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                     widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))
    end_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                   widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))

    class Meta:
        model = Task
        fields = ['name', 'long_name', 'status', 'asset_id', 'artists', 'step', 'start_date', 'end_date',
                  'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Task short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Task long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Task description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle task locked.'})
                   }


class ShotTaskForm(forms.ModelForm):
    step = forms.ModelChoiceField(queryset=Step.objects.filter(level='Shot'),
                                  widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                             'title': 'Task step.'}))
    shot_id = forms.IntegerField(widget=forms.HiddenInput())
    artists = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False,
                                             widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                                'title': 'Task artists.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Task}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                             'title': 'Applicable tags.'}))
    start_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                     widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))
    end_date = forms.DateTimeField(input_formats=['%Y-%m-%d'],
                                   widget=forms.DateTimeInput(format='%Y-%m-%d',
                                                              attrs={'data-date-format': 'yyyy-mm-dd',
                                                                     'data-plugin-datepicker': '',
                                                                     'class': 'form-control'}))

    class Meta:
        model = Task
        fields = ['name', 'long_name', 'status', 'shot_id', 'artists', 'step', 'start_date', 'end_date',
                  'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Task short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Task long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Task description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle task locked.'})
                   }


class ResourceForm(forms.ModelForm):
    task_id = forms.IntegerField(widget=forms.HiddenInput())
    artist = forms.ModelChoiceField(queryset=User.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Resource author.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Resource}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Resource
        fields = ['task_id', 'name', 'long_name', 'status', 'artist', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Resource short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Resource long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Resource description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle resource locked.'})
                   }


class VersionForm(forms.ModelForm):
    resource_id = forms.IntegerField(widget=forms.HiddenInput())
    artist = forms.ModelChoiceField(queryset=User.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Version author.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Version}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Version
        fields = ['resource_id', 'name', 'long_name', 'status', 'artist', 'description', 'for_dailies', 'tags',
                  'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Version short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Version long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Version description.'}),
                   'for_dailies': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                             'title': 'Toggle version for dailies.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle version locked.'})
                   }


class PlaylistForm(forms.ModelForm):
    project = forms.ModelChoiceField(queryset=Project.objects.all(), widget=forms.HiddenInput())
    file_ids = SimpleArrayField(forms.IntegerField(), widget=forms.HiddenInput())
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{Playlist}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Playlist status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = Playlist
        fields = ['name', 'long_name', 'status', 'description', 'tags', 'locked', 'project', 'file_ids']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'Version short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'Version long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'Version description.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle version locked.'})
                   }


class FileForm(forms.ModelForm):
    version_id = forms.IntegerField(widget=forms.HiddenInput())
    artist = forms.ModelChoiceField(queryset=User.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'File author.'}))
    status = forms.ModelChoiceField(queryset=Status.objects.filter(Q(applies_to__contains='{All}') |
                                                                   Q(applies_to__contains='{File}')),
                                    widget=forms.Select(attrs={'class': 'form-control mb-md ',
                                                               'title': 'Entity status.'}))
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          required=False,
                                          widget=forms.SelectMultiple(attrs={'class': 'form-control',
                                                                            'title': 'Applicable tags.'}))

    class Meta:
        model = File
        fields = ['version_id', 'name', 'long_name', 'status', 'artist', 'format',
                  'is_preview', 'description', 'tags', 'locked']
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control',
                                                  'title': 'File short name.'}),
                   'long_name': forms.TextInput(attrs={'class': 'form-control',
                                                       'title': 'File long name.'}),
                   'description': forms.TextInput(attrs={'class': 'form-control',
                                                         'title': 'File description.'}),
                   'format': forms.TextInput(attrs={'class': 'form-control',
                                                    'title': 'File format.'}),
                   'is_preview': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                             'title': 'Toggle file is a preview.'}),
                   'locked': forms.CheckboxInput(attrs={'class': 'checkbox-custom checkbox-primary',
                                                        'title': 'Toggle file locked.'})
                   }


MODELS_TO_FORMS = {
    'Software': SoftwareForm,
    'Module': ModuleForm,
    'Profile': ProfileForm,
    'Project': ProjectForm,
    'Episode': EpisodeForm,
    'Sequence': SequenceForm,
    'Shot': ShotForm,
    'AssetType': AssetTypeForm,
    'Category': CategoryForm,
    'Asset': AssetForm,
    'Step': StepForm,
    'Task': TaskForm,
    'Resource': ResourceForm,
    'Version': VersionForm,
    'File': FileForm,
    'User': UserForm,
    'EditType': EditTypeForm,
    'Edit': EditForm,
    'Status': StatusForm,
    'Playlist': PlaylistForm,
    'Department': DepartmentForm
}
