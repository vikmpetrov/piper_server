# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import re
from distutils.dir_util import copy_tree
from distutils.util import strtobool
from django.db import models
from django.db.models.query import QuerySet
from django.http import JsonResponse
from django.contrib.auth import authenticate
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from piper_core.api import DbApi
from piper_core.utils import string_to_json
from piper_core.models import *
from piper_core.config import PROTECTED_MODELS
from .serializers import *

UNAUTHORIZED_RESPONSE = Response({'error': 'Unauthorized access.', 'unauthorized': True}, 
                                 status=status.HTTP_400_BAD_REQUEST)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('username')
    serializer_class = UserSerializer


def _serializer_check(data):
    if type(data) == str and re.findall('{(.*?)}', data):
        data = string_to_json(data)
    return data


def _entity_check(data):
    if type(data) == dict and 'type' in data and 'id' in data:
        data = eval(data['type']).objects.get(id=data['id'])
    return data


@api_view(['GET', ])
def profiles(request):
    if request.method == 'GET':
        serializer = ProfileSerializer(Profile.objects.all(), many=True, context={'request': request},
                                       fields=['software'])
        return Response(serializer.data)


@api_view(['GET', ])
def user(request, username):
    if request.method == 'GET':
        user = User.objects.filter(username=username).first()
        if user:
            serializer = UserSerializer(user, context={'request': request},
                                        fields=['department', 'thumbnail_url', 'full_url'])
            return Response(serializer.data)
    return Response({}, status=status.HTTP_404_NOT_FOUND)


def _entity_permission_check(request, entity):
    if request.user.is_superuser:
        return True
    if hasattr(entity, 'created_by'):
        if entity.created_by == request.user.username:
            return True
    if entity.type == 'User' and request.user.id == entity.id:
        return True
    if request.user.is_anonymous:
        return False
    if _model_protected_check(request=request, entity_model=entity.type):
        return False
    if hasattr(entity, 'project'):
        if entity.project and entity.project not in request.user.projects.all():
            return False
    return True


def _model_protected_check(request, entity_model):
    return entity_model in PROTECTED_MODELS and not request.user.is_superuser


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def entity(request, entity_model, entity_id=None):
    if request.user.is_anonymous:
        return UNAUTHORIZED_RESPONSE
    try:
        model = eval(entity_model)
    except NameError:
        return Response({'error': 'Invalid entity type.', 'invalid': True}, status=status.HTTP_400_BAD_REQUEST)

    request_data = dict()
    serializer_data = dict()

    for key, value in request.data.items():
        if key != '_fields':
            if value.startswith('[') and value.endswith(']'):
                value_list = string_to_json(value)
                serialized_value = []
                request_data_value = []
                for element in value_list:
                    serialized_element = _serializer_check(data=element)
                    serialized_value.append(serialized_element)
                    request_data_value.append(_entity_check(serialized_element))
            else:
                serialized_value = _serializer_check(data=value)
                request_data_value = _entity_check(serialized_value)
            request_data[key] = request_data_value
            serializer_data[key] = serialized_value

    fields = get_fields(request)

    if entity_id:
        try:
            instance = model.objects.get(id=entity_id)
        except model.DoesNotExist:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

    db_api = DbApi()

    if request.method == 'GET':

        if entity_id:
            result = instance
            if 'attribute' in request_data:
                attribute = request_data['attribute']
                if hasattr(result, attribute):
                    value = getattr(result, attribute)
                    if value:
                        value_type = type(value)
                        if (value_type == QuerySet) or (value_type == list):
                            if hasattr(value[0], 'type'):
                                entity_model = getattr(value[0], 'type')
                                value = [MODEL_SERIALIZER[entity_model](x,
                                                                        context={'request': request},
                                                                        fields=fields).data for x in value]
                        elif issubclass(value_type, models.Model):
                            entity_model = getattr(value, 'type')
                            value = MODEL_SERIALIZER[entity_model](value,
                                                                   context={'request': request}, fields=fields).data
                        else:
                            pass
                    return Response({'value': value})
            serializer = MODEL_SERIALIZER[entity_model](result, context={'request': request}, fields=fields)

        elif request_data:
            result = model.objects.filter(**request_data)
            serializer = MODEL_SERIALIZER[entity_model](result, many=True, context={'request': request}, fields=fields)
        else:
            result = model.objects.all()
            serializer = MODEL_SERIALIZER[entity_model](result, many=True, context={'request': request}, fields=fields)
        return Response(serializer.data)

    elif request.method == 'POST':
        if _model_protected_check(request=request, entity_model=entity_model):
            return UNAUTHORIZED_RESPONSE
        instance, message = db_api.create_entity(entity_type=entity_model, **request_data)
        if not instance:
            return Response(message, status=status.HTTP_400_BAD_REQUEST)
        serializer = MODEL_SERIALIZER[entity_model](instance, context={'request': request}, fields=fields)
        db_api.email_notification_check(entity=instance, request=request, action='published')
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    elif request.method == 'PUT':
        if entity_id:
            if not _entity_permission_check(request=request, entity=instance):
                return UNAUTHORIZED_RESPONSE
            serializer = MODEL_SERIALIZER[entity_model](instance, data=serializer_data,
                                                        context={'request': request}, fields=request_data.keys())
            if serializer.is_valid():
                serializer.save(request_data)
                db_api.email_notification_check(entity=instance, request=request)
                db_api.sg_sync_check(entity=instance)
                return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        if entity_id:
            if not _entity_permission_check(request=request, entity=instance):
                return UNAUTHORIZED_RESPONSE
            db_api.email_notification_check(entity=instance, request=request, action='deleted')
            db_api.sg_sync_check(entity=instance, delete=True)
            instance.delete()
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)
        return Response({}, status=status.HTTP_204_NO_CONTENT)

    return Response({}, status=status.HTTP_400_BAD_REQUEST)


def get_fields(request):
    fields = []
    if '_fields' in request.data:
        request_fields = request.data['_fields']
    else:
        request_fields = request.GET.get('fields', '')
    if request_fields:
        request_fields = request_fields.split(',')
        fields += request_fields
    return list(set(fields))


@api_view(['GET'])
def children(request, parent_model, parent_id, child_model):
    if request.user.is_anonymous:
        return UNAUTHORIZED_RESPONSE
    try:
        model = eval(child_model)
    except NameError:
        return Response({'error': 'Invalid entity type.', 'invalid': True}, status=status.HTTP_400_BAD_REQUEST)

    fields = get_fields(request)

    result = model.objects.filter(_parent_model=parent_model, _parent_id=parent_id)
    serializer = MODEL_SERIALIZER[child_model](result, many=True, context={'request': request}, fields=fields)
    return Response(serializer.data)


@api_view(['GET'])
def assets(request, parent_model, parent_id, asset_type_id):
    if request.user.is_anonymous:
        return UNAUTHORIZED_RESPONSE

    result = Asset.objects.filter(_parent_model=parent_model, _parent_id=parent_id, asset_type_id=asset_type_id)
    serializer = AssetSerializer(result, many=True, context={'request': request})
    return Response(serializer.data)


@api_view(['GET'])
def steps(request, level):
    if request.user.is_anonymous:
        return UNAUTHORIZED_RESPONSE

    result = Step.objects.filter(level=level)
    serializer = StepSerializer(result, many=True, context={'request': request})
    return Response(serializer.data)


@api_view(['GET'])
def tasks(request, parent_model, parent_id, step_id):
    if request.user.is_anonymous:
        return UNAUTHORIZED_RESPONSE

    result = Task.objects.filter(step_id=step_id, _parent_model=parent_model, _parent_id=parent_id)
    serializer = TaskSerializer(result, many=True, context={'request': request})
    return Response(serializer.data)


@api_view(['GET'])
def browser_tree(request, project_name):
    project = Project.objects.get(name=project_name)

    if request.user and request.user.is_human and (request.user not in project.user_set.all()):
        if project.created_by != request.user.username:
            if not request.user.is_superuser:
                return JsonResponse({'status': 0,
                                     'message': 'Unauthorized action'},
                                    status=500)
    db_api = DbApi()
    tree = db_api.project_tree(project, serialized=True)

    return JsonResponse(tree, status=200)


@api_view(['GET'])
def active_dcc_tasks(request, artist_id, dcc_id):
    artist = User.objects.get(id=artist_id)
    dcc = Software.objects.get(id=dcc_id)
    tasks = [task for task in artist.active_tasks if task.step.category.name == dcc.category.name]
    serializer = UserTaskSerializer(tasks, many=True, context={'request': request}, fields=get_fields(request))
    return Response(serializer.data)


@api_view(['GET'])
def config(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    db_api = DbApi()
    if request.method == 'GET':
        config = db_api.get_config(entity)
        if config:
            config = MODEL_SERIALIZER['Config'](config)
        return JsonResponse(config, status=200)


@api_view(['GET'])
def test_authenticate(request, username, password):
    if authenticate(username=username, password=password):
        return JsonResponse({'status': 1, 'message': 'Successfully authenticated.'}, status=200)
    return JsonResponse({'status': 0, 'message': 'Username or password incorrect...'}, status=200)


@api_view(['POST'])
def publish(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    system = System.objects.get(platform=sys.platform)
    work_path = os.path.join(system.work_root, entity.work_path)
    publish_path = os.path.join(system.publish_root, entity.publish_path)
    db_api = DbApi()

    if os.path.exists(publish_path):
        return JsonResponse({'status': 1,
                             'message': '{0} {1} already published.'.format(entity_model, entity.long_name)},
                            status=200)

    if not os.path.exists(work_path):
        return JsonResponse({'status': 0,
                             'message': 'Publish failed because of invalid work path: {}'.format(work_path)},
                            status=500)

    if entity_model == 'File':
        parent_work_dir = os.path.dirname(work_path)
        parent_publish_dir = os.path.dirname(publish_path)
        copy_tree(parent_work_dir, parent_publish_dir)
    else:
        copy_tree(work_path, publish_path)

    db_api.email_notification_check(entity=entity, request=request, action='published')

    return JsonResponse({'status': 1,
                         'message': 'Successfully published {0} {1}.'.format(entity_model, entity.long_name)},
                        status=200)


@api_view(['POST'])
def file_rendered(request, file_id):
    file = File.objects.get(id=file_id)
    version = file.version
    completed_status = Status.objects.filter(name='completed').first()
    review_status = Status.objects.filter(name='review').first()
    if completed_status:
        updated_by = 'piper_client_api'
        if request.data and 'updated_by' in request.data:
            updated_by = request.data['updated_by']
        file.updated_by = updated_by
        file.status = completed_status
        file.save()
        if review_status:
            version.updated_by = updated_by
            version.status = review_status
            version.save()
        else:
            return JsonResponse({'status': 0, 'message': 'No "review" status found, failed to update version entity.'},
                                status=500)
        return JsonResponse({'status': 1,
                             'message': 'Set file status to "complete" and its version status to "review".'},
                            status=200)
    else:
        return JsonResponse({'status': 0, 'message': 'No "completed" status found, failed to update file entity.'},
                            status=500)


@api_view(['POST'])
def sync_to_sg(request, entity_model, entity_id):
    entity = eval(entity_model).objects.get(id=entity_id)
    db_api = DbApi()
    result, message = db_api.sync_to_sg(entity=entity)
    if result:
        return JsonResponse({'status': 1, 'message': message}, status=200)
    else:
        return JsonResponse({'status': 0, 'message': message}, status=500)


@api_view(['POST'])
def notify(request):
    db_api = DbApi()
    from_email = request.user.email
    if 'from_email' in request.data:
        from_email = request.data['from_email']
    verbose_email = False
    if 'verbose_email' in request.data:
        verbose_email = strtobool(request.data['verbose_email'])
    warnings = db_api.notify(target=request.data['recipients'],
                             subject=request.data['subject'],
                             message=request.data['message'],
                             from_email=from_email,
                             verbose_email=verbose_email)
    return JsonResponse({'status': 1,
                         'warnings': warnings,
                         'message': 'Notification completed.'},
                        status=200)


@api_view(['GET'])
def get_inherited_config(request, entity_model, entity_id):
    config = Config.objects.filter(_parent_model=entity_model, _parent_id=entity_id).first()
    if not config:
        config = Config(_parent_model=entity_model, _parent_id=entity_id)

    config_rules = config.inherited_config_rules

    return JsonResponse(config_rules,
                        status=200)
