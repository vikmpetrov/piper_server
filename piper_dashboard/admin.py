# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import UserForm
from piper_core.models import User


class CustomUserAdmin(UserAdmin):
    model = User
    add_form = UserForm
    form = UserForm


admin.site.register(User, CustomUserAdmin)
