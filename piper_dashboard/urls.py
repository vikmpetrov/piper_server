# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from rest_framework import routers
from . import views
from . import rest


# Authentication patterns:
auth_patterns = [
    path('login/', views.sign_in, name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('lock/', views.lock, name='lock'),
    path('unlock/', views.unlock, name='unlock'),
    path('recover/', views.recover, name='recover'),
    path('reset-password/', views.reset_password, name='reset_password'),
]


urlpatterns = [
    path('', views.home, name='home'),
    path('projects/refresh/', views.projects_refresh),
    path('accounts/', include(auth_patterns)),
    path('notes/', views.notes_inbox),
    path('notes/sent/', views.notes_outbox),
    path('notes/trash/', views.notes_trash),
    path('notes/refresh/', views.notes_refresh),
    path('notes/<str:note_type>/refresh/', views.notes_refresh),
    path('notes/<str:note_type>/<str:note_filter>/', views.notes_refresh),
    path('correspondence/<int:note_id>/', views.mail_correspondence),
    path('studio/config/globals/', views.studio_config_globals),
    path('studio/config/systems/', views.studio_config_systems),
    path('studio/config/users/', views.studio_config_users),
    path('studio/config/production/', views.studio_config_production),
    path('search/', views.search_everywhere),
    path('search/notes/<str:note_type>/<str:note_filter>/', views.search_notes),
    path('error/403/', views.handler403),
    path('error/404/', views.handler404),
    path('error/500/', views.handler500),
    path('error/502/', views.handler502),
    path('media-gallery/', views.media_gallery),
    path('media-gallery/elements/', views.media_gallery_elements),
    path('media-gallery/elements/<int:index_start>/<int:index_end>/', views.media_gallery_elements),
    path('video/File/<int:file_id>/', views.display_video)
]


# Multiple entity-related URL patterns:
urlpatterns += [
    path('Software/', views.software),
    path('Software/<int:software_id>/Module/', views.module),
    path('Module/', views.module),
    path('Profile/', views.profile),
    path('Department/', views.department),
    path('Department/<int:department_id>/User/', views.users),
    path('Project/', views.project),
    path('Project/<int:project_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/Sequence/<int:sequence_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/',
         views.entities),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/<str:_parent_model>/<str:_parent_id>/' +
         '<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/<str:_parent_model>/' +
         '<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/Resource/<int:resource_id>' +
         '/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/',
         views.entities),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/<str:_parent_model>/' +
         '<str:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/' +
         '<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/' +
         'Resource/<int:resource_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/Episode/<int:episode_id>/Sequence/<int:sequence_id>/<str:link_model>/' +
         '<int:link_id>/Task/<int:task_id>/Resource/<int:resource_id>/<str:_parent_model>/<int:_parent_id>/' +
         '<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:_parent_model>/<str:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:task_model>/<int:task_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.entities),
    path('Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:task_model>/<int:task_id>/<str:resource_model>/<int:resource_id>/<str:_parent_model>/<int:_parent_id>/' +
         '<str:entity_model>/', views.entities),
]


# Specific entity-related URL patterns:
urlpatterns += [
    path('jsonify/<str:entity_model>/<int:entity_id>/', views.jsonify),
    path('Task/<int:task_id>/log/', views.log_work),
    path('browser/details/<str:entity_model>/<int:entity_id>/', views.browser_details),
    path('browser/project/<str:project_name>/', views.browser_tree),
    path('Software/<int:software_id>/', views.software),
    path('Software/<int:software_id>/Module/<int:module_id>/', views.module),
    path('Profile/<int:profile_id>/', views.profile),
    path('Module/<int:module_id>/', views.module),
    path('Department/<int:department_id>/', views.department),
    path('History/<str:_parent_model>/<int:_parent_id>/', views.history),
    path('Department/<int:department_id>/User/<int:user_id>/', views.user),
    path('Project/<int:project_id>/', views.project),
    path('Project/<int:project_id>/<str:entity_model>/<int:entity_id>/', views.entity, name='episode'),
    path('Project/<int:project_id>/<str:entity_model>/<int:entity_id>/', views.entity, name='sequence'),
    path('Project/<int:project_id>/<str:entity_model>/<int:entity_id>/', views.entity, name='asset'),
    path('Project/<int:project_id>/<str:entity_model>/<int:entity_id>/', views.entity, name='playlist'),
    path('Project/<int:project_id>/Sequence/<int:sequence_id>/<str:entity_model>/<int:entity_id>/',
         views.entity, name='shot'),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/<str:entity_model>/<int:entity_id>/',
         views.entity, name='task'),
    path('Project/<int:project_id>/Sequence/<int:sequence_id>/<str:link_model>/<int:link_id>/<str:entity_model>/' +
         '<int:entity_id>/', views.entity, name='task'),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/<str:entity_model>/' +
         '<int:entity_id>/', views.entity, name='resource'),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/Resource/<int:resource_id>' +
         '/<str:entity_model>/<int:entity_id>/', views.entity, name='version'),
    path('Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/Resource/<int:resource_id>' +
         '/Version/<int:version_id>/<str:entity_model>/<int:entity_id>/', views.entity, name='file'),
    path('Project/<int:project_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/<int:entity_id>/',
         views.entity, name='episodic_sequence'),
    path('Project/<int:project_id>/Episode/<int:episode_id>/Sequence/<int:sequence_id>/<str:entity_model>/' +
         '<int:entity_id>/',  views.entity, name='episodic_shot'),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/<str:entity_model>/' +
         '<int:entity_id>/', views.entity, name='episodic_task'),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/' +
         '/<str:entity_model>/<int:entity_id>/', views.entity, name='episodic_resource'),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/' +
         'Resource/<int:resource_id/<str:entity_model>/<int:entity_id>/', views.entity, name='episodic_version'),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/' +
         'Resource/<int:resource_id>/Version/<int:version_id>/<str:entity_model>/<int:entity_id>/', views.entity,
         name='episodic_file'),
    path('Project/<int:project_id>/Episode/<int:episode_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/' +
         '<int:link_id>/Task/<int:task_id>/Resource/<int:resource_id>/<str:_parent_model>/<int:_parent_id>/' +
         '<str:entity_model>/<int:entity_id>/', views.entity),
    path('Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:_parent_model>/<str:_parent_id>/<str:entity_model>/<int:entity_id>/', views.entity),
    path('Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:task_model>/<int:task_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/<int:entity_id>/',
         views.entity),
    path('Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:task_model>/<int:task_id>/<str:resource_model>/<int:resource_id>/<str:_parent_model>/<int:_parent_id>/' +
         '<str:entity_model>/<int:entity_id>/', views.entity),
    path('configure/<str:entity_model>/<int:entity_id>/', views.configure),
    path('Profile/<int:profile_id>/software/', views.profile_software),
    path('activity/<int:index_start>/<int:index_end>/<str:entity_model>/<int:entity_id>/', views.activity)
]


# Data processing URL patterns:
urlpatterns += [
    path('export/xls/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.export_entities_table_xls,
         name='export_entities_table'),
    path('export/xls/<str:entity_model>/', views.export_entities_table_xls),
    path('update/xls/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.update_entities_table_xls),
    path('update/xls/<str:entity_model>/', views.update_entities_table_xls),
    path('update/<str:entity_model>/<int:entity_id>/', views.update_entity),
    path('update/thumbnail/<str:entity_model>/<int:entity_id>/', views.update_thumbnail),
    path('create/<str:entity_model>/', views.create_entity),
    path('create/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.create_entity),
    path('form/<str:entity_model>/<int:entity_id>/', views.entity_form),
    path('lock/<str:entity_model>/<int:entity_id>/', views.lock_entity),
    path('unlock/<str:entity_model>/<int:entity_id>/', views.unlock_entity),
    path('delete/<str:entity_model>/<int:entity_id>/', views.delete),
    path('refresh/table/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/', views.table_refresh),
    path('refresh/table/<str:entity_model>/', views.table_refresh),
    path('refresh/search/', views.search_refresh),
    path('studio/config/systems/update/<int:config_id>/', views.studio_config_systems),
    path('studio/config/globals/update/', views.studio_config_globals),
    path('note/post/<str:_parent_model>/<int:_parent_id>/', views.post_note),
    path('note/reply/<int:reply_to_id>/<str:_parent_model>/<int:_parent_id>/', views.post_note),
    path('note/thread/<int:note_id>/', views.note_thread_refresh),
    path('note/trash/<int:note_id>/', views.note_trash),
    path('Task/<int:task_id>/ingest/', views.ingest_resource),
    path('Task/<int:task_id>/re-time/<int:start_time>/<int:end_time>/', views.retime_task),
    path('User/<str:username>/add/Project/<int:project_id>/', views.add_artist_project),
    path('User/<str:username>/remove/Project/<int:project_id>/', views.remove_artist_project)
]

# Timeline patterns:
urlpatterns += [
    path('timeline/Project/<int:project_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_episode'),
    path('timeline/Project/<int:project_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_sequence'),
    path('timeline/Project/<int:project_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_asset'),
    path('timeline/Project/<int:project_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_playlist'),
    path('timeline/Project/<int:project_id>/Sequence/<int:sequence_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_shot'),
    path('timeline/Project/<int:project_id>/<str:link_model>/<int:link_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_task'),
    path('timeline/Project/<int:project_id>/Sequence/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:entity_model>/<int:entity_id>/', views.timeline, name='timeline_task'),
    path('timeline/Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/<str:entity_model>/' +
         '<int:entity_id>/', views.timeline, name='timeline_resource'),
    path('timeline/Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/Resource/' +
         '<int:resource_id>/<str:entity_model>/<int:entity_id>/', views.timeline, name='timeline_version'),
    path('timeline/Project/<int:project_id>/<str:link_model>/<int:link_id>/Task/<str:task_id>/Resource/' +
         '<int:resource_id>/Version/<int:version_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_file'),
    path('timeline/Project/<int:project_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_episodic_sequence'),
    path('timeline/Project/<int:project_id>/Episode/<int:episode_id>/Sequence/<int:sequence_id>/<str:entity_model>/' +
         '<int:entity_id>/', views.timeline, name='timeline_episodic_shot'),
    path('timeline/Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/' +
         '<str:entity_model>/<int:entity_id>/', views.timeline, name='timeline_episodic_task'),
    path('timeline/Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/' +
         '<str:task_id>/<str:entity_model>/<int:entity_id>/', views.timeline, name='timeline_episodic_resource'),
    path('timeline/Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/' +
         '<str:task_id>/Resource/<int:resource_id/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_episodic_version'),
    path('timeline/Project/<int:project_id>/Episode/<int:episode_id>/<str:link_model>/<int:link_id>/Task/' +
         '<str:task_id>/Resource/<int:resource_id>/Version/<int:version_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline, name='timeline_episodic_file'),
    path('timeline/Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:_parent_model>/<str:_parent_id>/<str:entity_model>/<int:entity_id>/', views.timeline),
    path('timeline/Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:task_model>/<int:task_id>/<str:_parent_model>/<int:_parent_id>/<str:entity_model>/<int:entity_id>/',
         views.timeline),
    path('timeline/Project/<int:project_id>/<str:sequence_model>/<int:sequence_id>/<str:link_model>/<int:link_id>/' +
         '<str:task_model>/<int:task_id>/<str:resource_model>/<int:resource_id>/<str:_parent_model>/<int:_parent_id>/' +
         '<str:entity_model>/<int:entity_id>/', views.timeline),
    path('timeline/refresh/<str:entity_model>/<int:entity_id>/', views.timeline_refresh)
]

# Tools URL patterns:
urlpatterns += [
    path('tools/browser/', views.browser),
    path('tools/task-manager/', views.task_manager),
    path('tools/task-manager/create/', views.task_manager_create),
    path('tools/resource-ingestor/', views.resource_ingestor),
    path('tools/resource-ingestor/batch/', views.batch_ingest_from_xls),
]

# Spreadsheet templates patterns:
urlpatterns += [
    path('spreadsheet/resource-ingestor/', views.spreadsheet_resource_ingestor_template),
]

# RESTful API patterns:
router = routers.DefaultRouter()
router.register(r'users', rest.UserViewSet)

urlpatterns += [
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/<str:entity_model>/', rest.entity),
    path('api/<str:entity_model>/<int:entity_id>/', rest.entity),
    path('api/Step/<str:level>/', rest.steps),
    path('api/<str:parent_model>/<int:parent_id>/Step/<int:step_id>/', rest.tasks),
    path('api/<str:parent_model>/<int:parent_id>/<str:child_model>/', rest.children),
    path('api/<str:parent_model>/<int:parent_id>/AssetType/<int:asset_type_id>/Asset/', rest.assets),
    path('api/project/<str:project_name>/tree/', rest.browser_tree),
    path('api/<str:entity_model>/<int:entity_id>/config/', rest.config),
    path('api/studio/profiles/', rest.profiles),
    path('api/User/<str:username>/', rest.user),
    path('api/User/<int:artist_id>/Software/<int:dcc_id>/tasks/', rest.active_dcc_tasks),
    path('api/authenticate/<str:username>/<str:password>/', rest.test_authenticate),
    path('api/publish/<str:entity_model>/<int:entity_id>/', rest.publish),
    path('api/rendered/File/<int:file_id>/', rest.file_rendered),
    path('api/sync/<str:entity_model>/<int:entity_id>/', rest.sync_to_sg),
    path('api/config/<str:entity_model>/<int:entity_id>/', rest.get_inherited_config),
    path('api/notify/email/', rest.notify)
]
