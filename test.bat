echo Starting Piper Server...
@echo off
set PIPER_DEBUG_MODE=1
set PIPER_SERVER_ROOT=%~dp0
call %PIPER_SERVER_ROOT%\venv\Scripts\activate.bat
python manage.py makemigrations
python manage.py migrate
python manage.py test