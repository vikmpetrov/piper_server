echo Setting up Piper Server, please wait...
@echo off
set PIPER_SERVER_ROOT=%~dp0
set PIPER_OS=win32
cd %PIPER_SERVER_ROOT%\resources\win32
call setup_python.bat
call setup_apache.bat
call setup_shotgun_api3.bat
set MOD_WSGI_APACHE_ROOTDIR=%PIPER_SERVER_ROOT%\resources\win32\Apache24
cd %PIPER_SERVER_ROOT%
echo Setting up virtual environment...
%PIPER_SERVER_ROOT%\resources\win32\Python37\python.exe -m pip install virtualenv
%PIPER_SERVER_ROOT%\resources\win32\Python37\python.exe -m virtualenv venv -p %PIPER_SERVER_ROOT%resources\win32\Python37\python.exe
call %PIPER_SERVER_ROOT%\venv\Scripts\activate.bat
pip install -r requirements.txt
echo Piper Server setup completed.
pause