echo Starting Piper Server...
export PIPER_DEBUG_MODE="0"
export PIPER_SERVER_ROOT=$PWD
source venv/bin/activate
python manage.py makemigrations
python manage.py migrate
echo Running Piper Server on Apache
$PIPER_SERVER_ROOT/resources/darwin/Apache24/bin/apachectl stop -f $PIPER_SERVER_ROOT/resources/darwin/Apache24/conf/httpd.conf
$PIPER_SERVER_ROOT/resources/darwin/Apache24/bin/apachectl start -f $PIPER_SERVER_ROOT/resources/darwin/Apache24/conf/httpd.conf
echo Piper Server is now running.