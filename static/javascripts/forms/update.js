var stack_top_left = {'dir1': 'down', 'dir2': 'right', 'push': 'top'};
var stack_bottom_left = {'dir1': 'right', 'dir2': 'up', 'push': 'top'};
var stack_bottom_right = {'dir1': 'up', 'dir2': 'left', 'firstpos1': 15, 'firstpos2': 15};
var stack_bar_top = {'dir1': 'down', 'dir2': 'right', 'push': 'top', 'spacing1': 0, 'spacing2': 0};
var stack_bar_bottom = {'dir1': 'up', 'dir2': 'right', 'spacing1': 0, 'spacing2': 0};
var entity_config_loaded = false;

$('.update-form').on('submit', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $form = $(this);
    return update_entity($form);
});

function update_entity($form) {
    $.ajax({
        url : $form.attr('action'),
        type : 'POST',
        data : $form.serialize(),

        // handle a successful response
        success: function(json) {
            update_success(json);
            if ($('#editForm').data('callback')) {
                eval($('#editForm').data('callback'));
            }
        },

        // handle a non-successful response
        error : update_error
    });
    return false;
};

$('.create-form').on('submit', function(event){
    event.preventDefault();
    $form = $(this);
    return create_entity($form);
});

function create_entity($form) {
    $.ajax({
        url : $form.attr('action'),
        type : 'POST',
        data : $form.serialize(),

        // handle a successful response
        success: function(json) {
            update_success(json);
            if ($form.data('refresh')) {
                refresh_div('#' + $form.data('refresh'), null, $form.data('callback'));
            } else {
                if ($form.data('callback')) {
                    eval($form.data('callback'));
                }
            }
            $form.find("input[type=text]").val("");
        },

        // handle a non-successful response
        error : update_error
    }).done(function() {
        refresh_table();
    });
    $.magnificPopup.close();
    return false;
};

$('.update-from-spreadsheet').on('submit', function(event){
    event.preventDefault();
    $form = $(this);
    batch_update_from_spreadsheet($form);
    refresh_table();
});

function batch_update_from_spreadsheet($form) {
    var form_data = new FormData($form[0]);

    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: form_data,
        processData: false,
        contentType: false,
        success: update_success,
        error: update_error
    }).done(function() {
        refresh_table();
    });
    $.magnificPopup.close();
    return false;
}

function update_error(xhr, errmsg, err) {
    console.log(xhr.status + ' : ' + xhr.responseText);
    console.log(errmsg + ' : ' + err);
    var notice = new PNotify({
        title: 'Error',
        text: 'Update failed: ' + jQuery.parseJSON(xhr.responseText)['message'],
        type: 'error',
        addclass: 'stack-bottomright',
        stack: stack_bottom_right
    });
}

function update_success(json) {
    if ('warnings' in json) {
        for (var i = 0; i < json.warnings.length; i++) {
            var notice = new PNotify({
            title: 'Warning',
            text: json.warnings[i],
            type: 'warning',
            addclass: 'stack-bottomright',
            stack: stack_bottom_right});
            console.log(json.warnings[i])
        }
    }
    var success_message = json.message;
    var notice = new PNotify({
        title: 'Success',
        text: success_message,
        type: 'success',
        addclass: 'stack-bottomright',
        stack: stack_bottom_right
    });
    console.log(success_message);
}

$('#datatable-editable').on('click', '.locker', function(){
    var url;
    var lock;

    if ($(this).hasClass('fa-unlock')) {
        $(this).removeClass('fa-unlock');
        $(this).addClass('fa-lock');
        url = '/lock/' + $(this).attr('id')
        lock = true;
    } else {
        $(this).removeClass('fa-lock');
        $(this).addClass('fa-unlock');
        url = '/unlock/' + $(this).attr('id')
        lock = false;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: {'lock': lock, csrfmiddlewaretoken: getCookie('csrftoken')},
        success: update_success,
        error: update_error
    })
});

function delete_entity_row($row) {
    var url = '/delete/' + $row.attr('id')
    return delete_entity(url);
}

function delete_entity($url) {
    $.ajax({
        url: $url,
        type: 'POST',
        data: {'delete': true, csrfmiddlewaretoken: getCookie('csrftoken')},
        success: update_success,
        error: update_error
    })
    return false;
}

$('#refreshButton').click(function(){
    return refresh_table();
});

function refresh_table() {
    if ($.fn.dataTable != null) {
        if ( $.fn.dataTable.isDataTable( '#datatable-editable' ) ) {
            table = $('#datatable-editable').DataTable();
        }
        else {
            table = $('#datatable-editable').DataTable( {
                paging: false
            } );
        }

        $.ajax({
            url: '/refresh/table/' + $('#datatable-editable').data('entities'),
                success: function(json) {
                    // clear table data:
                    table.clear();

                    // insert new data:
                    var data;
                    var $row;
                    for (var i = 0; i < json.data.length; i++) {
                        data = table.row.add(json.data[i]).draw();
                        $row = table.row( data[0] ).nodes().to$();
                        $row.addClass( 'adding' ).find( 'td:last' ).addClass( 'actions' );
                    }
                    console.log('Refreshed data table.');
            }
        });
    }
    return false;
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$('#post-note-form').on('submit', function(event){
    event.preventDefault();
    $form = $(this);
    return post_note($form);
});

function post_note($form) {
    var data = new FormData($form.get(0));
    data.append('content',  $('#post-note').val());
    data.append('tags', $('#tags-input').val());
    data.append('csrfmiddlewaretoken', getCookie('csrftoken'));

    $.ajax({
        url: $form.attr('action'),
        type: 'POST',
        data: data,
        success: update_success,
        error: update_error,
        cache: false,
        contentType: false,
        processData: false
    }).done(function() {
        $('#post-note').val('');
        $('#tags-input').tagsinput('removeAll');
        $('#id_file').val('')
        $('#selected-files').val('')
        refresh_div('#activity-timeline');
    });

    return false;
}

function refresh_div(div_id, action, callback) {
    var div = $(div_id);
    var url;
    if (action) {
        url = action;
    } else {
        url = div.data('action');
    }
    $.ajax({
        url: url,
        type: 'GET',
        success: function(json) {
            div.fadeOut(function() {
                div.html(json['data']).fadeIn(function() {
                    if (callback) {
                        eval(callback);
                    }
                });
            })
        }
    });
    console.log('Refreshed div:', div_id)
    return false;
}

function refresh_edit_form(action)  {
    var url = '/form' + action;
    $.ajax({
        url: url,
        type: 'GET',
        success: function(json) {
            $('#editForm').html(json['data']);
            $('#updateFormCSRF').val(getCookie('csrftoken'));
            $('#editFormTrigger').trigger('click');
        }
    })
}

$('.datatable-editable').on('click', '.edit-button', function(event){
    return refresh_edit_form($(this).data('entity'));
});

$('#editForm').on('click', '.form-delete-btn', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $.when( delete_entity($(this).data('action')) ).done(function() {
        if ($('#editForm').data('callback')) {
            eval($('#editForm').data('callback'));
        }
    });
    return false;
});

$('#editForm').on('submit', '.update-form', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $form = $(this);
    var updating = $.post($form.attr('action'), $form.serialize() );
    updating.success(function( data ) {
        update_success(data);
        if ($('#editForm').data('callback')) {
            eval($('#editForm').data('callback'));
        }
        refresh_table();
    });
    updating.error(function( data ) {
        update_error(data);
    });
    return false;
});

$('.log-work-btn').on('click', function(event) {
    $('#logWorkTrigger').trigger('click');
});

$('.add-configuration-row').on('click', function(event) {
    event.preventDefault();
    $('#own-configuration-table > tbody:last-child').append('<tr><td contenteditable></td><td contenteditable></td></tr>');
});

function config_dialog(url) {
    if (!entity_config_loaded) {
        $.ajax({
            url: url,
            type: 'GET',
            success: function(json) {
                var json_keys = Object.keys(json);
                if (json) {
                    if (json[1]) {
                        $.each(json[1], function( key, value ) {
                            $('#own-configuration-table > tbody:last-child').append('<tr><td contenteditable>' + key + '</td><td contenteditable>' + value + '</td></tr>');
                        });
                    }
                    if (json_keys.length>1) {
                        for (var i = 2; i <= json_keys.length; i++) {
                            if (json[i]) {
                                $.each(json[i].data, function( key, value ) {
                                    $('#inherited-configuration-table > tbody:last-child').append("<tr><td><a href='" + json[i].url + "' target='_blank'>" + json[i].name + "</a></td><td>" + json[i].type + "</td><td>" + key + "</td><td>" + value + "</td></tr>");
                                });
                            }
                        }
                    }
                }
                entity_config_loaded = true;
                $('#own-configuration-table > tbody:last-child').append('<tr><td contenteditable></td><td contenteditable></td></tr>');
            }
        })
    }
    $('#configureFormTrigger').trigger('click');
}

$('.configure-btn').on('click', function(event) {
    return config_dialog($(this).data('action'));
});

$('.configure-action').on('click', function(event) {
    $("#own-configuration-table td").each(function() {
        $(this).remove();
    });
    $("#inherited-configuration-table td").each(function() {
        $(this).remove();
    });
    entity_config_loaded = false;
    var action = $(this).data('action');
    $("#configure-form").attr('action', '/configure/' + action)
    $("#configure-title").text(' Configure <b>' + $(this).data('entity') + '</b>')
    return config_dialog('/api/config/' + action);
});

$('.update-thumbnail-form').on('submit', function(event){
    event.preventDefault();
    $form = $(this);
    var form_data = new FormData($form[0]);
    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: form_data,
        processData: false,
        contentType: false,
        success: function(json) {
            update_success(json);
            $('#thumbnail').fadeOut(function() {
                $('#thumbnail').html(json['data']).fadeIn();
            });
        },
        error: update_error
    });
    $.magnificPopup.close();
    return false;
});

$(function () {
    //Set up ajax error handling:
    $.ajaxSetup({
        error: function (x, status, error) {
            console.log(x + ' '+ status + ' '+ error)
            var notice = new PNotify({
                title: 'Error',
                text: 'Unexpected error: ' + error,
                type: 'error',
                addclass: 'stack-bottomright',
                stack: stack_bottom_right
            });
        }
    });
});

$('.log-work-form').on('submit', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $form = $(this);
    $.ajax({
        url : '/Task/' + $('#log-work-form').data('task') + '/log/',
        type : 'POST',
        data : $form.serialize(),

        // handle a successful response
        success: function(json) {
            update_success(json);
            $('#hours-logged').fadeOut(function() {
                var result = parseFloat($('#hours-logged').text()) + parseFloat($form.find('input[name="duration"]').val());
                $('#hours-logged').text(result).fadeIn();
            });
        },

        // handle a non-successful response
        error : update_error
    });
    return false;
});

$('.configure-form').on('submit', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $form = $(this);
    var config_rules = {};
    var key;
    $('#own-configuration-table td').each(function (i, row) {
        if (i % 2 === 0) {
            value = $(row).text()
            if (value) {
                key = value;
            } else {
                key = null;
            }
        } else {
            if (key) {
                config_rules[key] = $(row).text()
            }
        }
    });
    $.ajax({
        url : $form.attr('action'),
        type : 'POST',
        data : {'config_rules': JSON.stringify(config_rules), csrfmiddlewaretoken: getCookie('csrftoken')},
        success : update_success,
        error : update_error
    });
    return false;
});

$('.form-close').click(function(){
    $.magnificPopup.close();
});

$('#editForm').on('click', '.form-close', function(event){
    $.magnificPopup.close();
});

$(document).keyup(function(e) {
     if (e.key === "Escape") {
        $.magnificPopup.close();
    }
});