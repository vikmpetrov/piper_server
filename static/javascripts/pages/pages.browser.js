var filter_by_user = null;
var filtered_nodes = [];
var user_filtered_nodes = [];

$( document ).ready(function() {
    $('.selectpicker').selectpicker();
});

$('.entity-tree').on('click', '.entity-branch', function(event){
    return display_details(this);
});

$('.entity-tree').on('click', '.project-branch', function(event){
    return display_details(this);
});

function display_details(selected) {
    $.ajax({
        url: $(selected).data('action'),
        type: 'GET',
        success: function(json) {
            $('#entity-details').fadeOut(function() {
                $('#entity-details').html(json['data']).fadeIn();
            });
        }
    })
}

$( "#project-selector" ).change(function() {
    $('#project-load-spinner').fadeIn();
    console.log($( this ).val())
    if ($( this ).val()) {
        $.ajax({
            url: '/browser/project/' + $(this).val(),
            type: 'GET',
            success: function(json) {
                $('#project-tree').jstree(true).settings.core.data = json['data'];
                $('#project-tree').jstree(true).redraw(true);
                $('#project-tree').jstree(true).refresh();
            }
        })
    } else {
        $('#project-tree').jstree(true).settings.core.data = '<ul><li class="colored"></li></ul>';
        $('#project-tree').jstree(true).redraw(true);
        $('#project-tree').jstree(true).refresh();
    }
    $('#entity-details').fadeOut(function() {
        $('#entity-details').html('<p><div class="post-image appear-animation bounceIn appear-animation-visible centered"><div class="img-thumbnail"><img src="/media/thumbnails/default_thumbnail.png" class="img-responsive"></div></div></p>').fadeIn();
    });

    $('#filters-accordion input[type=checkbox]').each(function(i, checkbox) {
        if (!checkbox.checked) {
            $(checkbox).fadeOut(function() {
                $(checkbox).prop("checked", true).fadeIn();
            });
        }
    });

    $('#user-filter-checkbox').fadeOut(function() {
        $('#user-filter-checkbox').prop("checked", false).fadeIn();
    });

    filter_by_user = null;
    filtered_nodes = [];
    user_filtered_nodes = [];

    if ($(this).val()) {
        $(document).ajaxStop(function () {
            $('#project-load-spinner').fadeOut();
        });
    } else {
        $('#project-load-spinner').fadeOut();
    }
});

$('.asset-type-checkbox').change(function() {
    return filter(this, 'asset-branch', 'data-type');
});

$('.status-checkbox').change(function() {
    return filter(this, 'entity-branch', 'data-status');
});

$('.category-checkbox').change(function() {
    return filter(this, 'resource-branch', 'data-category');
});

$('#user-filter-refresh').on('click', function(event){
    return user_filter();
});

$('#user-filter-checkbox').change(function() {
    return user_filter();
});

$('#user-filter-input').change(function() {
    return user_filter();
});

function user_filter() {
    if ($("#user-filter-checkbox").is(":checked")) {
        filter_by_user = $('#user-filter-input').val();
        var $tree = $('#project-tree');
        $($tree.jstree().get_json($tree, {
            flat: true
        }))
        .each(function(index, node) {
            if ((node['a_attr']['class']) && (node['a_attr']['class'].indexOf('entity-branch') != -1)) {
                if (node['a_attr']['data-author'] != filter_by_user) {
                    if ($.inArray(node, filtered_nodes) < 0) {
                        $("#project-tree").jstree(true).hide_node(node);
                        user_filtered_nodes.push(node);
                    }
                } else {
                    if ($.inArray(node, filtered_nodes) < 0) {
                        $("#project-tree").jstree(true).show_node(node);
                    }
                }
            }
        });
    } else {
        $.each(user_filtered_nodes, function(index, node) {
            $("#project-tree").jstree(true).show_node(node);
        });
        $.each(filtered_nodes, function(index, node) {
            $("#project-tree").jstree(true).hide_node(node);
        });
        user_filtered_nodes = [];
        filter_by_user = null;
    }
}

function filter(checkbox, branch, data) {
    var checked = checkbox.checked;
    var value = $("label[for='"+$(checkbox).attr("id")+"']").text().toLowerCase();
    var $tree = $('#project-tree');
    var index;
    $($tree.jstree().get_json($tree, {
        flat: true
    }))
    .each(function(index, node) {
        if ((node['a_attr']['class']) && (node['a_attr']['class'].indexOf(branch) != -1)) {
            if (node['a_attr'][data] == value) {
                if(checked) {
                    $("#project-tree").jstree(true).show_node(node);
                    index = filtered_nodes.indexOf(node);

                    $.each(filtered_nodes, function(index, filtered_node) {
                        if (node['id'] == filtered_node['id']) {
                            filtered_nodes.splice(index, 1);
                            return false;
                        }
                    });
                } else {
                    $("#project-tree").jstree(true).hide_node(node);
                    filtered_nodes.push(node);
                }
            }
        }

    });
}
