var activity_index = 10;
var activity_last = $( "li.timeline-activity" ).last();
var activity_loading = false;
var activity_maxed_out = false;

(function( $ ) {

	'use strict';

	var initLightbox = function() {
		$('.timeline .thumbnail-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
			}
		});
	};

	$(function() {
		initLightbox();
	});

}).apply(this, [ jQuery ]);

$( document ).on('scroll', function() {
    if (activity_maxed_out) {
        return
    }
    if (activity_loading) {
        return
    }
    if ($(this).scrollTop() >= activity_last.position().top) {
        activity_loading = true;
        $('#loading-spinner').fadeIn();
        activity_index += 25;
        var activity_index_end = activity_index + 25;
        $.ajax({
            url: '/activity/' + activity_index + '/' + activity_index_end + $('#timeline').data('entity'),
            type: 'GET',
            success: function(json) {
                $('#loading-spinner').fadeOut();
                if (json['data']) {
                    $('#timeline-items').append(json['data']);
                    activity_last = $( "li.timeline-activity" ).last();
                } else {
                    activity_maxed_out = true;
                }
                activity_loading = false;
            }
        })
    }
});

$("#activity-timeline").on("click", ".filter-checkbox", function(){
    if (this.checked) {
        $( ".activity-" + $(this).data('activity') ).show("slow");
    } else {
        $( ".activity-" + $(this).data('activity') ).hide("slow");
    }
});

function setup_logging_data() {
    if (typeof logged_work_chart_data != 'undefined') {
        return create_work_log_chart();
    }
}

function create_work_log_chart() {
    var logged_work_chart = $.plot('#chart-work-logged', logged_work_chart_data, {
        series: {
            lines: {
                show: true,
                fill: true,
                lineWidth: 1,
                fillColor: {
                    colors: [{
                        opacity: 0.45
                    }, {
                        opacity: 0.45
                    }]
                }
            },
            points: {
                show: true
            },
            shadowSize: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderColor: 'rgba(0,0,0,0.1)',
            borderWidth: 1,
            labelMargin: 15,
            backgroundColor: 'transparent'
        },
        yaxis: {
            min: logged_work_chart_duration_min,
            max: logged_work_chart_duration_max,
            color: 'rgba(0,0,0,0.1)'
        },
        xaxis: {
            mode: "time",
            timezone: "browser",
            color: 'rgba(0,0,0,0)'
        },
        tooltip: true,
        tooltipOpts: {
            content: function(label, x_value, y_value) {
                return '%s logged ' + y_value + ' hours: "' + logged_work_logs_data[x_value]['description'] + '"';
            },
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        }
    });
}

$( document ).ready(function() {
	setup_logging_data();
    $('#minimize-logged-work-chart').trigger('click');
    $('.select2-container').select2();
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});

function get_random_color() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function add_remove_artist_project(action, event) {
    var username = event.val;
    var project_id = $('#artist-select').data('project-id');
    $.ajax({
        url: '/User/' + username + '/' + action + '/Project/' + project_id + '/',
        type: 'POST',
        data : {csrfmiddlewaretoken: getCookie('csrftoken')},
        success : update_success,
        error : update_error
    })
}

$('#artist-select').on('select2-selecting', function (event) {
    add_remove_artist_project('add', event);
});

$('#artist-select').on('select2-removing', function (event) {
    add_remove_artist_project('remove', event);
});
