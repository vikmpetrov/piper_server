$('#search-btn').on('click', function(event) {
    return refresh_search();
});

$('#search-form').on('submit', function(event) {
    event.preventDefault();
    return refresh_search();
});

function refresh_search() {
    $('#search-results').fadeOut(function() {
        $('#search-load-spinner').fadeIn();
        var url = '/refresh/search/?q=' + $('#search-query').val();
        $.ajax({
            url: url,
            type: 'GET',
            success: function(json) {
                $('#search-load-spinner').fadeOut(function() {
                    $('#search-results').html(json['data']).fadeIn();
                });
            }
        });
    });
    return false;
}
