
(function( $ ) {

	'use strict';

	var initLightbox = function() {
		$('.timeline .thumbnail-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
			}
		});
	};

	$(function() {
		initLightbox();
	});

}).apply(this, [ jQuery ]);

$("#activity-timeline").on("click", ".filter-checkbox", function(){
    if (this.checked) {
        $( ".activity-" + $(this).data('activity') ).show("slow");
    } else {
        $( ".activity-" + $(this).data('activity') ).hide("slow");
    }
});

