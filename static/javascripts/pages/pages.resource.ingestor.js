$( document ).ready(function() {
    $("#new-resource").prop('disabled', true);
});


$('#shot-select').on('change', function() {
    populate_steps();
});

function populate_steps () {
    clear_selectpicker('step-select');
    var level = get_pipeline_level();
    var url = '/api/Step/' + level + '/';
    if ($('#' + level.toLowerCase() + '-select').val()) {
        populate_selectpicker(url, 'step-select');
    }
}

$('#asset-select').on('change', function() {
    populate_steps();
});

$('#step-select').on('change', function() {
    clear_selectpicker('task-select');
    var level = get_pipeline_level();
    var url = '/api/' + level + '/' + $('#' + level.toLowerCase() + '-select').val() + '/Step/' + $('#step-select').val() + '/';
    populate_selectpicker(url, 'task-select');
    $('#logWorkForm').data('task', '')
});

$('#task-select').on('change', function() {
    clear_selectpicker('resource-select');
    var url = '/api/Task/' + $('#task-select').val() + '/Resource/';
    populate_selectpicker(url, 'resource-select');
    $('#logWorkForm').data('task', $('#task-select').val())
    $('#log-work-title').text('Log work for task ' + $('#task-select option:selected').text());
});

$('#resource-selection input').on('change', function() {
    var new_resource_status = false;
    var existing_resource_status = true;
    if ($(this).val() === 'resource-existing') {
        new_resource_status = true;
        existing_resource_status = false;
    }
    $("#new-resource").prop('disabled', new_resource_status);
    $("#resource-select").prop('disabled', existing_resource_status);
    $("#resource-select").selectpicker('refresh');
});

$('#log-work-btn').on('click', function(event){
    if ($('#task-select').val()) {
        $('#logWorkTrigger').trigger('click');
    } else {
        return input_check('#task-select');
    }
});

$('.ingest-from-spreadsheet').on('submit', function(event){
    event.preventDefault();
    $.magnificPopup.close();
    $('#app-icon').fadeOut(function() {
        $('#app-icon').removeClass('fa-file-picture-o').addClass('fa-spinner').addClass('fa-spin').fadeIn();
    });
    $form = $(this);
    var form_data = new FormData($form[0]);
    $.ajax({
        url: '/tools/resource-ingestor/batch/',
        type: 'POST',
        data: form_data,
        processData: false,
        contentType: false,
        success: update_success,
        error: update_error
    }).done(function() {
        $('#app-icon').fadeOut(function() {
            $('#app-icon').removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-file-picture-o').fadeIn();
        });
    });
    return false;
});
