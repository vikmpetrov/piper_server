var context_project_id = null;
var context_episode_id = null;
var context_asset_id = null;
var context_shot_id = null;

$( document ).ready(function() {
    $('.selectpicker').selectpicker();
});

function clear_selectpicker(id) {
    if (id == 'asset-select') {
        context_asset_id = null;
    }
    else if (id == 'shot-select') {
        context_shot_id = null;
    } else if (id == 'episode-select') {
        context_episode_id = null;
        context_asset_id = null;
        context_shot_id = null;
    }
    $('#' + id).find('[value!=""]').remove();
    $('#' + id).selectpicker('refresh');
}

$('#project-select').on('change', function() {
    clear_selectpicker('sequence-select');
    clear_selectpicker('shot-select');
    clear_selectpicker('asset-select');
    clear_selectpicker('episode-select');
    var project_entity = {};
    var value = this.value;
    context_project_id = value;
    $.ajax({
        url: '/api/Project/' + value + '/?fields=episodic',
        type: 'GET',
        success: function(data) {
            project_entity = data;
        }
    }).done(function() {
        if (project_entity.episodic) {
            $('#episode-select-grp').fadeIn();
            populate_selectpicker('/api/Project/' + value + '/Episode/', 'episode-select');
        } else {
            $('#episode-select-grp').fadeOut();
            populate_selectpicker('/api/Project/' + value + '/Sequence/', 'sequence-select');
        }
        populate_assets();
    });
});

$('#episode-select').on('change', function() {
    clear_selectpicker('sequence-select');
    clear_selectpicker('shot-select');
    clear_selectpicker('asset-select');
    context_episode_id = this.value;
    var url = '/api/Episode/' + context_episode_id + '/Sequence/';
    populate_selectpicker(url, 'sequence-select');
    populate_assets()
});

$('#sequence-select').on('change', function() {
    clear_selectpicker('shot-select');
    var url = '/api/Sequence/' + this.value + '/Shot/';
    populate_selectpicker(url, 'shot-select');
});

$('#shot-select').on('change', function() {
    context_shot_id = this.value;
});

$('#asset-select').on('change', function() {
    context_asset_id = this.value;
});

function populate_assets() {
    if ($('#asset-type-select').val()) {
        var url;
        if ($('#episode-select').val()) {
            url = '/api/Episode/' + $('#episode-select').val() + '/AssetType/' + $('#asset-type-select').val() + '/Asset/';
        } else {
            url = '/api/Project/' + $('#project-select').val() + '/AssetType/' + $('#asset-type-select').val() + '/Asset/';
        }
        populate_selectpicker(url, 'asset-select');
    }
}

$('#asset-type-select').on('change', function() {
    clear_selectpicker('asset-select');
    populate_assets();
});

function get_pipeline_level() {
    var level;
    if ($("#shot-tab").hasClass("active")) {
        level = 'Shot';
    } else {
        level = 'Asset';
    }
    return level;
}

function populate_selectpicker(url, id) {
    $.ajax({
        url: url,
        type: 'GET',
        success: function(data) {
            $.each(data, function (i, item) {
                $('#' + id).append($('<option>', {
                    value: item.id,
                    text : item.long_name
                }));
                $('#' + id).selectpicker('refresh');
            });
        }
    })
}
