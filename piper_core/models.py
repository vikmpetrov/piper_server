# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import re
import calendar
import operator
import datetime
import pytz
from tzlocal import get_localzone
from glob import glob
from itertools import chain
from django.conf import settings
from django.db import models
from django.db.utils import IntegrityError
from django.core.serializers import serialize
from django.utils import timezone
from django.contrib.auth.models import AbstractUser, UserManager
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.postgres.fields import JSONField, ArrayField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
from timecode import Timecode
from .utils import get_current_user, string_to_json, timecode_match, path_prefix_sanity_check, path_suffix_sanity_check
from .config import SUPPORTED_IMAGE_EXTENSIONS, SUPPORTED_VIDEO_EXTENSIONS, DATE_FORMAT, DATE_TIME_FORMAT, \
    TIMESTAMP_FORMAT
from .config.database import DEFAULT_PUBLISH_PATH_TEMPLATE, DEFAULT_WORK_PATH_TEMPLATE, VERSION_FILE_NAME_TEMPLATE


class MethodsMixin(object):
    """
    Class that contains key methods that all database entities should have by default.
    """
    _parent = None  # a private variable to store the entity's parent;
    children_models = []  # a variable to store the entity's children type;
    _work_path = None
    _publish_path = None
    _data_path = None
    _config = None

    def jsonify(self):
        """
        This method loops through all of the entity's attributes
        and returns them and their respective values as a dictionary.
        :return: key-value Python dictionary.
        """
        return string_to_json(serialize(format='json', queryset=[self]))[0]

    @property
    def parent(self):
        """
        A method to get this entity's parent entity.
        :return: a single entity, if it exists, otherwise None.
        """
        if not self._parent:
            if hasattr(self, '_parent_id') and self._parent_id:
                self._parent = eval(self._parent_model).objects.get(id=self._parent_id)
        return self._parent

    @parent.setter
    def parent(self, value):
        """
        A method to set this entity's parent entity.
        :param value: the entity to set as parent;
        :return: None.
        """
        if hasattr(self, '_parent_id') and hasattr(self, '_parent_model'):
            setattr(self, '_parent_id', value.id)
            setattr(self, '_parent_model', value.type)
            self._parent = value

    @property
    def children(self):
        """
        A method to get this entity's child entities.
        :return: a list of all of this entity's children-entities.
        """
        children = []
        for child_type in self.children_models:
            entity = eval(child_type)
            children += list(entity.objects.filter(_parent_id=self.id, _parent_model=self.type))
        return children

    @property
    def config(self):
        """
        A method to get this entity's config if it exists.
        :return: a single entity, if it exists, otherwise None.
        """
        if not self._config:
            self._config = Config.objects.filter(_parent_model=self.type, _parent_id=self.id).last()
        return self._config

    @property
    def type(self):
        """
        A property to easily access this entity's class name, which also represents its type.
        :return: the name of this entity's class as a string.
        """
        return self.__class__.__name__

    @property
    def work_path(self):
        if not self._work_path:
            self._work_path = self.generate_path(path_template='work_path_template')
        return self._work_path

    @property
    def publish_path(self):
        if not self._publish_path:
            self._publish_path = self.generate_path(path_template='publish_path_template')
        return self._publish_path

    @property
    def data_path(self):
        if self.publish_path and not self._data_path:
            self._data_path = '/data/{}'.format(self.publish_path)
        return self._data_path

    def generate_path(self, path_template, splitter='/'):
        path = ''
        if self.project:
            splice_index = 1
            if self.type != 'Project' and self.config:
                path_template = self.config.get_top_configuration(attribute=path_template)
            else:
                path_template = getattr(self.project, path_template)
            path = path_template
            path_elements = re.findall('{(.*?)}', path)
            path_elements.reverse()
            replaced_elements = []
            entity = self
            while entity:
                if len(replaced_elements) >= len(path_elements):
                    break
                entity_type = entity.type.lower()
                if entity_type in ['asset', 'shot']:
                    entity_type = 'link'
                if entity_type in path.lower():
                    for path_element in path_elements:
                        if entity_type in path_element and path_element not in replaced_elements:
                            value_element = path_element.split('.', 1)[-1]
                            entity_attribute_value = entity.safe_get(value_element)
                            if entity.type == 'Asset' and value_element in ['name', 'long_name', 'id']:
                                path = path.replace(path_element, 'assets/{}'.format(entity_attribute_value))
                                splice_index += 1
                            else:
                                path = path.replace(path_element, entity_attribute_value)
                            replaced_elements.append(path_element)
                entity = entity.parent

            if replaced_elements:
                path_template_elements = path_template.split(splitter)
                try:
                    index = path_template_elements.index('{'+replaced_elements[0]+'}')
                    path = splitter.join(path.split(splitter)[:index+splice_index])
                except ValueError:
                    pass

            path = path.replace('{', '').replace('}', '')
            if len(replaced_elements) < len(path_elements):
                for path_element in path_elements:
                    if path_element not in replaced_elements:
                        path = path.replace(path_element, '')

        return re.sub('\{}+'.format(splitter), splitter, path)

    def generate_file_name(self):
        return self.generate_path(path_template='file_name_template', splitter='_')

    def safe_get(self, attribute):
        try:
            return operator.attrgetter(attribute)(self)
        except (AttributeError, ValueError):
            return ''

    def date_time_strptime(self, attribute):
        value = self.safe_get(attribute)
        if value:
            value = timezone.localtime(value)
            return value.strftime(DATE_TIME_FORMAT)
        return ''

    def date_time_timestamp(self, attribute):
        value = self.safe_get(attribute)
        if value:
            return value.timestamp() * 1000

    @property
    def creation_date_timestamp(self):
        value = self.safe_get('created')
        if value:
            value = timezone.localtime(value)
            return value.strftime(TIMESTAMP_FORMAT)
        return ''

    @property
    def fields(self):
        return dict((field, self.safe_get(field)) for field in self.jsonify()['fields'])


class GlobalsConfig(models.Model, MethodsMixin):
    name = models.CharField(max_length=100, null=True, blank=True)
    studio_name = models.CharField(max_length=100, null=True, blank=True)
    start_frame = models.IntegerField(default=1001)
    auto_update_thumbnails = models.BooleanField(default=True)
    email_on_update = models.BooleanField(default=False)
    email_on_publish = models.BooleanField(default=False)
    email_host = models.TextField(null=True, blank=True)
    email_port = models.IntegerField(null=True, blank=True)
    email_username = models.EmailField(null=True, blank=True)
    email_password = models.TextField(null=True, blank=True)
    ticket_email = models.EmailField(null=True, blank=True)
    shotgun_sync = models.BooleanField(default=False)
    shotgun_url = models.TextField(null=True, blank=True)
    shotgun_script_name = models.TextField(null=True, blank=True)
    shotgun_application_key = models.TextField(null=True, blank=True)

    @property
    def email_ready(self):
        return self.email_host and self.email_port and self.email_username and self.email_password

    @property
    def can_sg_sync(self):
        return self.shotgun_url and self.shotgun_script_name and self.shotgun_application_key


class Model(models.Model):
    created = models.DateTimeField(default=timezone.now)  # notes when this entity was created;
    updated = models.DateTimeField(auto_now=True)  # notes when this entity was last updated;

    class Meta:
        abstract = True


class Tag(Model):
    type = 'Tag'
    name = models.CharField(max_length=100)
    link_id = models.IntegerField(null=True)
    metadata = JSONField(null=True, blank=True)
    created_by = models.CharField(max_length=100, default=get_current_user)

    def __str__(self):
        return 'Tag {}'.format(self.name)

    @property
    def url(self):
        return '/search/?q={}'.format(self.name)


class TimeMixin(object):

    @property
    def time_delta_label(self):
        delta = timezone.now() - self.created
        days, hours, minutes = delta.days, delta.seconds // 3600, (delta.seconds // 60) % 60
        label = ''
        if days:
            label += '{} days '.format(days)
        if hours:
            label += '{} hours '.format(hours)
        if minutes:
            label += '{} minutes'.format(minutes)
        if not label:
            label = 'A few seconds'
        label += ' ago'
        return label

    @property
    def weekday(self):
        return calendar.day_name[self.created.weekday()]


class FootprintMixin(models.Model, MethodsMixin):
    _project = models.ForeignKey('Project', null=True, on_delete=models.SET_NULL)
    _asset = models.ForeignKey('Asset', null=True, on_delete=models.SET_NULL)
    _episode = models.ForeignKey('Episode', null=True, on_delete=models.SET_NULL)
    _sequence = models.ForeignKey('Sequence', null=True, on_delete=models.SET_NULL)
    _shot = models.ForeignKey('Shot', null=True, on_delete=models.SET_NULL)
    _task = models.ForeignKey('Task', null=True, on_delete=models.SET_NULL)
    _resource = models.ForeignKey('Resource', null=True, on_delete=models.SET_NULL)
    _version = models.ForeignKey('Version', null=True, on_delete=models.SET_NULL)
    _file = models.ForeignKey('File', null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        setattr(self, '_{}'.format(self.type.lower()), self)
        parent = self.parent
        while parent:
            setattr(self, '_{}'.format(parent.type.lower()), parent)
            parent = parent.parent
        super().save(*args, **kwargs)  # Call the "real" save() method.


class History(FootprintMixin, Model, TimeMixin):
    _parent_model = models.CharField(max_length=50)
    _parent_id = models.IntegerField(null=True)
    before = JSONField(null=True, blank=True)
    after = JSONField(null=True, blank=True)
    created_by = models.CharField(max_length=100, default=get_current_user)

    _change = {}

    @property
    def url(self):
        return '/History/{0}/{1}/'.format(self._parent_model, self._parent_id)

    @property
    def change(self):
        if not self._change:
            before = self.before
            after = self.after
            if not before:
                self._change = {'before': before,
                                'after': after}
                return self._change
            for key, value in before.items():
                if key != 'updated':
                    if after[key] != value:
                        self._change = {'before': {key: value},
                                        'after': {key: after[key]}}
        return self._change


class Base(Model, MethodsMixin):
    """
    Class that contains key attributes that all database entities should have by default.
    """
    def __str__(self):
        # Get the name of this class as a string;
        return '{0} {1}'.format(self.type, self.long_name)

    name = models.CharField(max_length=100)  # the name of the entity in the database;
    long_name = models.CharField(max_length=100, blank=True)  # the entity's name for display purposes;
    description = models.TextField(null=True, blank=True)  # description of the entity;
    sg_id = models.IntegerField(null=True)  # the ID of a corresponding entity on SG;
    metadata = JSONField(null=True, blank=True)  # a dictionary to store any additional arbitrary data;
    _parent_model = models.CharField(max_length=50, null=True)  # the name of this entity's designated parent entity;
    _parent_id = models.IntegerField(null=True)  # the unique ID of the entity's parent;
    created_by = models.CharField(max_length=100, default=get_current_user)  # the name of the user that created this;
    updated_by = models.CharField(max_length=100)  # the name of the user that updates this;
    tags = models.ManyToManyField(Tag)  # tags for this entity;
    locked = models.BooleanField(default=False)  # locked for admin modification only;
    _original_fields = {}  # to store an initial snapshot of all entity fields;

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(Base, self).__init__(*args, **kwargs)
        if self.id:
            self._original_fields = self.jsonify()['fields']

    def rename(self):
        self.long_name = self.name.lower()

    def save(self, *args, **kwargs):
        if self.name:
            if not self.name.islower():
                self.name = self.name.lower()
        if not self.long_name:
            self.rename()
        if not self.description:
            self.description = 'Piper {}'.format(self.type)
        super().save(*args, **kwargs)  # Call the "real" save() method.
        self._original_fields = self._record_history()

    def _record_history(self):
        new_fields = self.jsonify()['fields']
        history = History(_parent_model=self.type,
                          _parent_id=self.id,
                          before=self._original_fields)

        if self._original_fields:
            mismatching_values = [value for key, value in self._original_fields.items()
                                  if key != 'updated' and (key in new_fields and value != new_fields[key])]
            if mismatching_values:
                history.after = new_fields
                history.save()
        else:
            history.after = {'Created': '{0} {1}'.format(self.type, self.name)}
            history.save()
        return new_fields

    @property
    def dependency(self):
        return Dependency.objects.filter(dependant_id=self.id, dependant_model=self.type).first()

    @property
    def history(self):
        return History.objects.filter(_parent_id=self.id, _parent_model=self.type).order_by('-created')

    @property
    def cascading_history(self):
        history = self.history
        for child in self.children:
            history |= child.cascading_history
        return history

    @property
    def activity(self):
        history = self.history
        if hasattr(self, 'history_set'):
            history |= self.history_set.all()
        notes = self.notes
        if hasattr(self, 'note_set'):
            notes |= self.note_set.all()
        activity = sorted(chain(history, notes), key=lambda instance: instance.created, reverse=True)
        return activity

    @property
    def url(self):
        if self.id:
            url_root = ''
            if not self._parent_model:
                url_root = '/'
            return '{0}{1}/{2}'.format(url_root, self.type, self.id)
        return None

    @property
    def full_url(self):
        url = self.url
        parent = self.parent
        while parent:
            url = os.path.join(parent.url, url).replace('\\', '/')
            parent = parent.parent
        return url

    @property
    def full_url_dirname(self):
        return os.path.dirname(self.full_url)

    @property
    def full_url_basename(self):
        return os.path.basename(self.full_url)

    @property
    def globals(self):
        try:
            return GlobalsConfig.objects.get(id=1)
        except ObjectDoesNotExist:
            return None

    @property
    def notes(self):
        return Note.objects.filter(_parent_id=self.id, _parent_model=self.type).order_by('-created')

    @property
    def parent_model(self):
        return self._parent_model

    @property
    def parent_id(self):
        return self._parent_id


class Status(Base):
    applies_to = ArrayField(models.CharField(max_length=100), default=['All'])
    is_active = models.BooleanField(default=True)

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Applies to': {'value': self.applies_to_str, 'attribute': 'applies_to'},
                'Is active': {'value': self.is_active, 'attribute': 'is_active'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'},
                }

    @property
    def applies_to_str(self):
        if self.applies_to:
            return ', '.join(self.applies_to)

    @property
    def full_url(self):
        return '/studio/config/production/'


class StatusMixin(models.Model):
    status = models.ForeignKey(Status, default=1, on_delete=models.PROTECT)

    class Meta:
        abstract = True


class ThumbnailMixin(models.Model):
    thumbnail = models.ImageField(upload_to='thumbnails', default='thumbnails/default_thumbnail.png')
    thumbnail_spec = ImageSpecField(source='thumbnail',
                                    processors=[ResizeToFill(200, 200)],
                                    format='JPEG',
                                    options={'quality': 75})
    user_set_thumbnail = models.BooleanField(default=False)

    @property
    def thumbnail_url(self):
        return self.thumbnail.url

    @property
    def thumbnail_spec_url(self):
        return self.thumbnail_spec.url

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.user_set_thumbnail and self.thumbnail and self.parent:
            if self.parent.type not in ['Project', 'Episode', 'Sequence', 'Department', 'System']:
                globals = GlobalsConfig.objects.get(id=1)
                if globals.auto_update_thumbnails and self.parent.thumbnail != self.thumbnail:
                    self.parent.thumbnail = self.thumbnail
                    self.parent.save()
        super().save(*args, **kwargs)  # Call the "real" save() method.


class Department(Base, StatusMixin, ThumbnailMixin):
    email = models.EmailField(null=True)
    children_models = ['User']

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'E-mail': {'value': self.email, 'attribute': 'email'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class Project(Base, StatusMixin, ThumbnailMixin):
    episodic = models.BooleanField(default=False)
    publish_path_template = models.TextField(default=DEFAULT_PUBLISH_PATH_TEMPLATE)
    work_path_template = models.TextField(default=DEFAULT_WORK_PATH_TEMPLATE)
    file_name_template = models.TextField(default=VERSION_FILE_NAME_TEMPLATE)

    def save(self, *args, **kwargs):
        if '\\' in self.publish_path_template:
            self.publish_path_template = self.publish_path_template.replace('\\', '/')
        if '\\' in self.work_path_template:
            self.work_path_template = self.work_path_template.replace('\\', '/')
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def project(self):
        return self

    @property
    def episodes(self):
        return self.episode_set.all()

    @property
    def assets(self):
        return self.asset_set.all()

    @property
    def sequences(self):
        return self.sequence_set.all()

    @property
    def children_models(self):
        if self.episodic:
            return ['Episode', 'Sequence', 'Asset']
        return ['Sequence', 'Asset']

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Episodic': {'value': self.episodic, 'attribute': 'episodic'},
                'Publish path template': {'value': self.publish_path_template, 'attribute': 'publish_path_template'},
                'Work path template': {'value': self.work_path_template, 'attribute': 'work_path_template'},
                'File name template': {'value': self.file_name_template, 'attribute': 'file_name_template'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class CustomUserManager(UserManager):
    pass


class User(Base, StatusMixin, ThumbnailMixin, AbstractUser):
    objects = CustomUserManager()
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(blank=True)
    department = models.ForeignKey(Department, on_delete=models.SET_NULL, null=True)
    projects = models.ManyToManyField(Project)
    is_human = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = self.username
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def tasks(self):
        return self.task_set.all()

    @property
    def active_tasks(self):
        return [task for task in self.tasks if task.is_active]

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Username': {'value': self.username, 'attribute': 'username'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'E-mail': {'value': self.email, 'attribute': 'email'},
                'Department': {'value': self.safe_get('department.long_name'),
                               '_id': self.safe_get('department.id'),
                               '_model': self.safe_get('department.type'),
                               'url': self.safe_get('department.full_url'),
                               'attribute': 'department'},
                'Is human': {'value': self.is_human, 'attribute': 'is_human'},
                'Is superuser': {'value': self.is_superuser, 'attribute': 'is_superuser'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}

    @property
    def mentions(self):
        return Note.objects.filter(mentioned_users__in=[self]).order_by('-created')

    @property
    def unseen_notes(self):
        return self.mentions.exclude(seen_by__in=[self])

    @property
    def activity(self):
        history = History.objects.filter(created_by=self.username).exclude(_parent_model='User').order_by('-created')
        notes = self.note_set.all()
        activity = sorted(chain(history, notes), key=lambda instance: instance.created, reverse=True)
        return activity


class Episode(Base, StatusMixin, ThumbnailMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    duration = models.TimeField(null=True, blank=True)
    children_models = ['Sequence', 'Asset']

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Duration': {'value': self.duration, 'attribute': 'duration'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class Sequence(Base, StatusMixin, ThumbnailMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    episode = models.ForeignKey(Episode, on_delete=models.SET_NULL, null=True)
    children_models = ['Shot']

    def save(self, *args, **kwargs):
        if not self.project and not self.episode:
            raise IntegrityError('A sequence requires either a project or an episode.')
        if not self._parent_id:
            parent = self.episode or self.project
            self._parent_model = parent.type
            self._parent_id = parent.id
        if not self.project:
            self.project = self.episode.project
        super().save(*args, **kwargs)

    @property
    def shots(self):
        return self.shot_set.all()

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class AssetType(Base):

    @property
    def display_fields(self):
        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            }
        return display_fields

    @property
    def full_url(self):
        return '/studio/config/production/'


class Category(Base):

    @property
    def display_fields(self):
        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            }
        return display_fields

    @property
    def full_url(self):
        return '/studio/config/production/'


class Asset(Base, StatusMixin, ThumbnailMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    episode = models.ForeignKey(Episode, on_delete=models.SET_NULL, null=True)
    asset_type = models.ForeignKey(AssetType, on_delete=models.PROTECT)
    children_models = ['Task']

    def save(self, *args, **kwargs):
        if not self.project and not self.episode:
            raise IntegrityError('An asset requires either a project or an episode.')
        if not self._parent_id:
            parent = self.project
            if self.episode:
                parent = self.episode
            self._parent_model = parent.type
            self._parent_id = parent.id
        if not self.project:
            self.project = self.episode.project
        super().save(*args, **kwargs)
        prefix = '{}_'.format(self.asset_type.name)
        if not self.long_name.startswith(prefix):
            self.long_name = prefix + self.long_name
            super().save(*args, **kwargs)

    @property
    def tasks(self):
        return self.task_set.all()

    @property
    def resources(self):
        resources = Resource.objects.filter(task__in=self.task_set.all())
        return resources

    @property
    def sequence(self):
        class AssetSequence(dict):
            def __init__(self):
                dict.__init__(self, name='assets', long_name='assets', type='Sequence')
                self.name = 'assets'
                self.long_name = 'assets'
                self.type = 'Sequence'

        return AssetSequence()

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Asset type': {'value': self.safe_get('asset_type.long_name'),
                               '_id': self.safe_get('asset_type.id'),
                               '_model': self.safe_get('asset_type.type'),
                               'url': self.safe_get('asset_type.full_url'),
                               'attribute': 'asset_type'},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class Shot(Base, StatusMixin, ThumbnailMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    sequence = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    children_models = ['Task']
    camera_data = JSONField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    pre_roll = models.IntegerField(default=0)
    post_roll = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.sequence.project
        super().save(*args, **kwargs)

    @property
    def episode(self):
        return self.sequence.episode

    @property
    def tasks(self):
        return self.task_set.all()

    @property
    def resources(self):
        resources = Resource.objects.filter(task__in=self.task_set.all())
        return resources

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Duration': {'value': self.duration, 'attribute': 'duration'},
                'Pre-roll': {'value': self.pre_roll, 'attribute': 'pre_roll'},
                'Post-roll': {'value': self.post_roll, 'attribute': 'post_roll'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class Step(Base, StatusMixin):
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    level = models.CharField(max_length=100)

    def __str__(self):
        return 'Step {0} ({1})'.format(self.long_name, self.level)

    @property
    def display_fields(self):
        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Category': {'value': self.safe_get('category.long_name'), 'attribute': 'category'},
                            'Level': {'value': self.level, 'attribute': 'level'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            }
        return display_fields

    @property
    def full_url(self):
        return '/studio/config/production/'


class Task(Base, StatusMixin, ThumbnailMixin):
    step = models.ForeignKey(Step, on_delete=models.SET_NULL, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True, blank=True)
    episode = models.ForeignKey(Episode, on_delete=models.SET_NULL, null=True, blank=True)
    asset = models.ForeignKey(Asset, on_delete=models.SET_NULL, null=True)
    shot = models.ForeignKey(Shot, on_delete=models.SET_NULL, null=True)
    artists = models.ManyToManyField(User)
    start_date = models.DateTimeField(default=timezone.now)
    end_date = models.DateTimeField(null=True, blank=True)
    children_models = ['Resource']
    _link = None

    def save(self, *args, **kwargs):
        if not self.asset and not self.shot and not self._parent_id:
            raise IntegrityError('A task requires either a shot or an asset.')
        if not self._parent_id:
            parent = self.shot or self.asset
            self._parent_model = parent.type
            self._parent_id = parent.id
        if not self.project:
            self.project = self.parent.project
        if not self.episode:
            self.episode = self.parent.episode
        if not self.end_date:
            self.end_date = self.start_date + datetime.timedelta(days=1)
        super().save(*args, **kwargs)

    @property
    def resources(self):
        return self.resource_set.all()

    @property
    def artists_set(self):
        return self.artists.all()

    @property
    def artists_list(self):
        return [artist.username for artist in self.artists.all()]

    @property
    def link(self):
        if not self._link:
            if self.asset:
                self._link = self.asset
            else:
                self._link = self.shot
        return self._link
    
    @link.setter
    def link(self, value):
        if value.type == 'Asset':
            self.asset = value
            self.shot = None
        else:
            self.shot = value
            self.asset = None
        self.save()
        self._link = None

    @property
    def is_active(self):
        if not self.start_date:
            return False
        elif self.start_date and not self.end_date:
            return True
        else:
            now = timezone.now()
            if (self.start_date <= now and self.end_date >= now) and self.status.is_active:
                return True
        return False

    @property
    def hours_logged(self):
        duration = 0
        for work_log in self.worklog_set.all():
            duration += work_log.duration
        return duration

    @property
    def work_logs(self):
        return self.worklog_set.all().order_by('created')

    @property
    def work_logs_by_duration(self):
        return self.work_logs.order_by('-duration')

    @property
    def work_logs_by_user(self):
        users_to_logs = {}
        for log in self.work_logs:
            if log.artist.username not in users_to_logs:
                users_to_logs[log.artist.username] = []
            users_to_logs[log.artist.username].append(log)
        return users_to_logs

    @property
    def progress_percentage(self):
        if not self.end_date:
            return 0

        rest = self.end_date - timezone.now()
        total = self.end_date - self.start_date
        return 100.0 - round(100*(rest.total_seconds() / total.total_seconds()))

    @property
    def display_fields(self):
        start_date = ''
        end_date = ''
        if self.start_date:
            start_date = self.start_date.date().strftime(DATE_FORMAT)
        if self.end_date:
            end_date = self.end_date.date().strftime(DATE_FORMAT)

        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                            'Step': {'value': self.safe_get('step.long_name'),
                                     '_id': self.safe_get('step.id'),
                                     '_model': 'Step',
                                     'attribute': 'step',
                                     'url': self.safe_get('step.full_url')},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Status': {'value': self.safe_get('status.long_name'),
                                       '_id': self.safe_get('status.id'),
                                       '_model': 'Status',
                                       'attribute': 'status',
                                       'url': self.safe_get('status.full_url')},
                            'Start date': {'value': start_date,
                                           'attribute': 'start_date'},
                            'End date': {'value': end_date,
                                         'attribute': 'end_date'},
                            'Assigned to': {'value': self.safe_get('artists_list'),
                                            'attribute': 'artists'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            'Project': {'value': self.safe_get('project.long_name'),
                                        '_id': self.safe_get('project.id'),
                                        '_model': 'Project',
                                        'url': self.safe_get('project.full_url'),
                                        'attribute': 'project'},
                            }

        if self.link:
            display_fields[self.link.type] = {'value': self.safe_get('link.long_name'),
                                             '_id': self.safe_get('link.id'),
                                             '_model': self.safe_get('link.type'),
                                             'url': self.safe_get('link.full_url'),
                                             'attribute': 'link'}
        else:
            display_fields['Link'] = None

        return display_fields


class Resource(Base, StatusMixin, ThumbnailMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    source = models.CharField(max_length=100, default='standalone')
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    artist = models.ForeignKey(User, on_delete=models.CASCADE)
    children_models = ['Version']
    _asset = None
    _shot = None
    _category = None
    _link = None

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.task.project
        super().save(*args, **kwargs)  # Call the "real" save() method.
        prefix = '{}_'.format(self.task.name)
        if not self.long_name.startswith(prefix):
            self.long_name = prefix + self.long_name
            super().save(*args, **kwargs)

    @property
    def category(self):
        if not self._category:
            self._category = self.task.step.category
        return self._category

    @property
    def versions(self):
        return self.version_set.all()

    @property
    def link(self):
        if not self._link:
            self._link = self.task.parent
        return self._link

    @property
    def display_fields(self):
        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Source': {'value': self.source, 'attribute': 'source'},
                            'Status': {'value': self.safe_get('status.long_name'),
                                       '_id': self.safe_get('status.id'),
                                       '_model': 'Status',
                                       'url': self.safe_get('status.full_url'),
                                       'attribute': 'status'},
                            'Artist': {'value': self.safe_get('artist.long_name'),
                                       '_id': self.safe_get('artist.id'),
                                       '_model': 'User',
                                       'url': self.safe_get('artist.full_url'),
                                       'attribute': 'artist'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            }
        return display_fields


class Version(Base, StatusMixin, ThumbnailMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    artist = models.ForeignKey(User, on_delete=models.CASCADE)
    for_dailies = models.BooleanField(default=False)
    children_models = ['File']

    @property
    def files(self):
        return self.file_set.all()

    @property
    def work_file(self):
        return self.file_set.filter(is_work_file=True).first()

    @property
    def preview(self):
        return self.file_set.filter(is_preview=True).last()

    @preview.setter
    def preview(self, value):
        for file in self.file_set.filter(is_preview=True):
            if file == value:
                file.is_preview = True
                file.save()
            elif file != value and file.is_preview:
                file.is_preview = False
                file.save()
            else:
                pass
    '''
    @property
    def for_dailies(self):
        return self._for_dailies

    @for_dailies.setter
    def for_dailies(self, value):
        self._for_dailies = value
        if not value and not self.id:
            return
        self.save()
        folder_name = '{0}_{1}_{2}_{3}_{4}'.format(self.resource.task.link.sequence.long_name,
                                                   self.resource.task.link.long_name,
                                                   self.resource.task.long_name,
                                                   self.resource.long_name,
                                                   self.long_name)
        if self.project.episodic and self.resource.task.link.type == 'Shot':
            folder_name = '{0}_{1}'.format(self.resource.task.link.sequence.episode.long_name, folder_name)
        system = System.objects.get(platform=sys.platform)
        symlink_path = os.path.join(system.dailies_root, self.project.long_name,
                                    timezone.now().strftime(DATE_FORMAT),
                                    folder_name)
        if value:
            os.symlink(self.publish_path, symlink_path)
        else:
            if os.path.exists(symlink_path):
                os.unlink(symlink_path)
    '''

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.resource.project
        if not self.name:
            last_version = self.resource.version_set.all().last()
            if last_version:
                self.name = 'v{0}'.format(str((int(last_version.name[-4:]) + 1)).zfill(4))
            else:
                self.name = 'v0001'
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def is_latest(self):
        latest_version = self.resource.version_set.all().last()
        if self.id == latest_version.id:
            return True
        return False

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Artist': {'value': self.safe_get('artist.long_name'),
                           '_id': self.safe_get('artist.id'),
                           '_model': 'User',
                           'url': self.safe_get('artist.full_url'),
                           'attribute': 'artist'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'For dailies': {'value': self.for_dailies, 'attribute': 'for_dailies'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class Dependency(models.Model, MethodsMixin):
    name = models.CharField(max_length=100, null=True)
    dependant_model = models.CharField(max_length=50)
    dependant_id = models.IntegerField()
    dependencies = JSONField(null=True, blank=True)
    created_by = models.CharField(max_length=100, default=get_current_user)

    @property
    def dependant(self):
        return eval(self.dependant_model).objects.get(id=self.dependant_id)

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = self.dependant.name
        super().save(*args, **kwargs)  # Call the "real" save() method.


class File(Base, StatusMixin, ThumbnailMixin, MethodsMixin):
    format = models.CharField(max_length=100)
    is_sequence = models.BooleanField(default=False)
    is_work_file = models.BooleanField(default=False)
    is_image = models.BooleanField(default=False)
    is_video = models.BooleanField(default=False)
    is_preview = models.BooleanField(default=False)
    version = models.ForeignKey(Version, on_delete=models.CASCADE)
    artist = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True, blank=True)
    asset = models.ForeignKey(Asset, null=True, on_delete=models.SET_NULL)
    episode = models.ForeignKey(Episode, null=True, on_delete=models.SET_NULL)
    sequence = models.ForeignKey(Sequence, null=True, on_delete=models.SET_NULL)
    shot = models.ForeignKey(Shot, null=True, on_delete=models.SET_NULL)
    task = models.ForeignKey(Task, null=True, on_delete=models.SET_NULL)
    resource = models.ForeignKey(Resource, null=True, on_delete=models.SET_NULL)
    _extension = None
    path = None  # to store the path to the file on disk;
    _range = []  # to store frame range if the file is a sequence;
    _elements = [] # to frame file paths if the file is a sequence;
    full_publish_path = None

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.version.project
        if self.format.lower() in SUPPORTED_IMAGE_EXTENSIONS:
            self.is_image = True
        elif self.format.lower() in SUPPORTED_VIDEO_EXTENSIONS:
            self.is_video = True
        if not self.name:
            self.name = self.generate_file_name()
        if not self.description:
            self.description = self.version.description
        if not self.is_preview:
            self.is_preview = self.is_video
        super().save(*args, **kwargs)  # Call the "real" save() method.
        suffix = '_{}'.format(self.id)
        if not self.name.endswith(suffix):
            self.name += suffix
            self.rename()
        if not self.tags:
            self.tags = self.version.tags
        parent = self.parent
        while parent:
            setattr(self, parent.type.lower(), parent)
            parent = parent.parent
        super().save(*args, **kwargs)

    @property
    def extension(self):
        if not self._extension:
            if self.is_sequence:
                self._extension = '.####.{}'.format(self.format)
            else:
                self._extension = '.{}'.format(self.format)
        return self._extension

    @property
    def full_name(self):
        return '{}{}'.format(self.name, self.extension)

    @property
    def elements(self):
        if not self._elements:
            system = System.objects.filter(platform=sys.platform).first()
            if not system:
                return
            if self.is_sequence:
                self._elements = sorted(glob(os.path.join(system.publish_root,
                                                          self.publish_path.replace('.####.', '.*.'))))
            else:
                self._elements = [self.publish_path]
        return self._elements

    @property
    def range(self):
        if not self._range:
            if self.is_sequence and self.elements:
                self._range = [self.elements[0].split('.')[-2], self.elements[-1].split('.')[-2]]
            else:
                self._range = [1, 1]
        return self._range

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.full_url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Artist': {'value': self.safe_get('artist.long_name'),
                           '_id': self.safe_get('artist.id'),
                           '_model': 'User',
                           'url': self.safe_get('artist.full_url'),
                           'attribute': 'artist'},
                'Format': {'value': self.format, 'attribute': 'format'},
                'Is preview': {'value': self.is_preview, 'attribute': 'is_preview'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class Config(Base, StatusMixin):
    config_rules = JSONField(null=True, blank=True)
    _inherited_config_rules = None

    def get_top_configuration(self, attribute):
        for key in reversed(list(self.inherited_config_rules)):
            if attribute in self.inherited_config_rules[key]:
                return self.inherited_config_rules[key][attribute]
        return None

    @property
    def inherited_config_rules(self):
        if not self._inherited_config_rules:
            globals = GlobalsConfig.objects.get(id=1)
            all_config_data = {0: {'start_frame': globals.start_frame},
                               1: self.config_rules}  # To contain all found config data.
            index = 2
            entity = self.parent.parent
            if entity and entity.project:
                all_config_data[0]['publish_path_template'] = entity.project.publish_path_template
                all_config_data[0]['work_path_template'] = entity.project.work_path_template
            # The following loop attempts to find any config data for the given entity and all of its parent entities:
            while entity:
                config = Config.objects.filter(_parent_model=entity.type, _parent_id=entity.id).first()
                # If such a config entity exists, store all of its data:
                if config:
                    all_config_data[index] = {}
                    all_config_data[index]['name'] = entity.long_name
                    all_config_data[index]['data'] = config.config_rules
                    all_config_data[index]['type'] = entity.type
                    all_config_data[index]['id'] = entity.id
                    all_config_data[index]['url'] = entity.full_url
                    index += 1
                # Move onto the entity's parent entity:
                entity = entity.parent
            self._inherited_config_rules = all_config_data
        return self._inherited_config_rules

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Status': {'value': self.safe_get('status.long_name'),
                           '_id': self.safe_get('status.id'),
                           '_model': self.safe_get('status.type'),
                           'url': self.safe_get('status.full_url'),
                           'attribute': 'status'},
                'Config rules': {'value': self.config_rules, 'attribute': 'config_rules'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class WorkLog(Base):
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True, blank=True)
    episode = models.ForeignKey(Episode, on_delete=models.SET_NULL, null=True, blank=True)
    artist = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    duration = models.FloatField()

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.task.project
        if not self.episode:
            self.episode = self.task.link.episode
        super().save(*args, **kwargs)

    @property
    def created_strftime(self):
        return self.date_time_strptime('created')

    @property
    def created_timestamp(self):
        return self.date_time_timestamp('created')

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Artist': {'value': self.safe_get('artist.long_name'),
                           '_id': self.safe_get('artist.id'),
                           '_model': 'User',
                           'url': self.safe_get('artist.full_url'),
                           'attribute': 'artist'},
                'Task': {'value': self.safe_get('task.long_name'),
                           '_id': self.safe_get('task.id'),
                           '_model': 'Task',
                           'url': self.safe_get('task.full_url'),
                           'attribute': 'task'},
                'Duration': {'value': str(datetime.timedelta(seconds=self.duration)), 'attribute': 'duration'},
                'Created': {'value': self.created_strftime, 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Description': {'value': self.description, 'attribute': 'description'}}


class System(models.Model, MethodsMixin):
    platform = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    long_name = models.CharField(max_length=50)
    work_root = models.TextField(default='/')
    publish_root = models.TextField(default='/')
    software_root = models.TextField(default='/')
    modules_root = models.TextField(default='/')

    def save(self, *args, **kwargs):
        self.work_root = path_suffix_sanity_check(self.work_root)
        self.publish_root = path_suffix_sanity_check(self.publish_root)
        self.software_root = path_suffix_sanity_check(self.software_root)
        self.modules_root = path_suffix_sanity_check(self.modules_root)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def __str__(self):
        return 'System {}'.format(self.name)

    @property
    def url(self):
        return '/'

    @property
    def full_url(self):
        return '/studio/config/systems/'

    @property
    def modules_paths_list(self):
        return self.modules_paths.split(';')


class Software(Base, ThumbnailMixin):
    system = models.ForeignKey(System, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    executable = models.TextField()
    module_environment_variable = models.CharField(max_length=100, null=True, blank=True)
    file_extensions = ArrayField(models.CharField(max_length=100), default=[], blank=True)
    render_formats = ArrayField(models.CharField(max_length=100), default=[], blank=True)
    command_line_renderer = models.TextField(null=True, blank=True)
    is_farm = models.BooleanField(default=False)

    def __str__(self):
        return 'Software {0} on {1}'.format(self.name, self.system.long_name)

    def save(self, *args, **kwargs):
        self.command_line_renderer = path_prefix_sanity_check(self.command_line_renderer)
        self.executable = path_prefix_sanity_check(self.executable)
        if self.thumbnail == 'thumbnails/default_thumbnail.png':
            dcc_icon = '{}.png'.format(re.sub('[^A-Za-z]+', '', self.name.lower()))
            thumbnails_dir = os.path.join(settings.MEDIA_ROOT, 'thumbnails')
            if dcc_icon in os.listdir(thumbnails_dir):
                self.thumbnail = 'thumbnails/{}'.format(dcc_icon)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def modules(self):
        return self.module_set.all()

    @property
    def file_extensions_str(self):
        if self.file_extensions:
            return ', '.join(self.file_extensions)

    @property
    def render_formats_str(self):
        if self.render_formats:
            return ', '.join(self.render_formats)

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.full_url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'System': {'value': self.safe_get('system.long_name'),
                           '_id': self.safe_get('system.id'),
                           '_model': self.safe_get('system.type'),
                           'url': self.safe_get('system.full_url'),
                           'attribute': 'system'},
                'Executable': {'value': self.executable, 'attribute': 'executable'},
                'File extensions': {'value': self.file_extensions_str, 'attribute': 'file_extensions'},
                'Render formats': {'value': self.render_formats_str, 'attribute': 'render_formats'},
                'Module environment variable': {'value': self.module_environment_variable,
                                                'attribute': 'file_extensions'},
                'Is farm': {'value': self.is_farm, 'attribute': 'is_farm'},
                'Command line renderer': {'value': self.command_line_renderer, 'attribute': 'command_line_renderer'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}
                }


class Module(Base, ThumbnailMixin):
    root = models.TextField()
    software = models.ManyToManyField(Software)

    def save(self, *args, **kwargs):
        self.root = path_prefix_sanity_check(self.root)
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Thumbnail': {'value': self.thumbnail_url, 'attribute': 'thumbnail_url', 'url': self.full_url},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Root directory': {'value': self.root, 'attribute': 'root'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}
                }


class Profile(Base, ThumbnailMixin):
    software = models.ManyToManyField(Software)
    modules = models.ManyToManyField(Module)

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'}
                }


class Playlist(Base, ThumbnailMixin, StatusMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    episode = models.ForeignKey(Episode, on_delete=models.SET_NULL, null=True)
    file_ids = ArrayField(models.IntegerField(), blank=True, default=[])

    @property
    def files(self):
        return [File.objects.get(id=int(id)) for id in self.file_ids]

    def save(self, *args, **kwargs):
        if not self._parent_id:
            self._parent_id = self.project.id
            self._parent_model = self.project.type
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def display_fields(self):
        return {'ID': {'value': self.id, 'attribute': 'id'},
                'Name': {'value': self.long_name, 'attribute': 'long_name'},
                'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                'Description': {'value': self.description, 'attribute': 'description'},
                'File IDs': {'value': self.file_ids, 'attribute': 'file_ids'}
                }


class Note(FootprintMixin, Model, TimeMixin, MethodsMixin):
    content = models.TextField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    _parent_model = models.CharField(max_length=50)
    _parent_id = models.IntegerField()
    mentioned_users = models.ManyToManyField(User, related_name='note_mentioned_users')
    tags = models.ManyToManyField(Tag)
    seen_by = models.ManyToManyField(User, related_name='note_seen_by')
    trashed_by = models.ManyToManyField(User, related_name='note_trashed_by')
    reply_to = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    _attachments = None
    _seen = False

    @property
    def project(self):
        if self.parent:
            return self.parent.project
        return None

    @property
    def created_by(self):
        return self.author.name

    @property
    def attachments(self):
        if not self._attachments:
            self._attachments = Attachment.objects.filter(_parent_model=self.type, _parent_id=self.id)
        return self._attachments

    @property
    def thread(self):
        thread = Note.objects.filter(id=self.id)
        thread |= Note.objects.filter(reply_to=self).order_by('-created')
        return thread

    @property
    def seen(self):
        if not self.mentioned_users:
            self._seen = True
        if not self._seen:
            if set(self.mentioned_users.all()).issubset(set(self.seen_by.all())):
                self._seen = True
        return self._seen

    @property
    def tags_label(self):
        return ','.join([tag.name for tag in self.tags.all()])

    @property
    def full_url(self):
        return '/correspondence/{}/'.format(self.id)


class TaskTemplate(Base):
    tasks = JSONField(null=True, blank=True)


class EditType(Base):
    @property
    def display_fields(self):
        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            }
        return display_fields

    @property
    def full_url(self):
        return '/studio/config/production/'


class Edit(Base, StatusMixin, ThumbnailMixin):
    edit_type = models.ForeignKey(EditType, on_delete=models.PROTECT)
    duration = models.TimeField(null=True)
    frames = models.IntegerField(null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    episode = models.ForeignKey(Episode, on_delete=models.SET_NULL, null=True)
    sequence = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    timecode_start = models.CharField(max_length=50)
    timecode_end = models.CharField(max_length=50)
    fps = models.FloatField(default=24.0)
    edl = models.FileField(upload_to='edls/')
    items = ArrayField(models.CharField(max_length=100), blank=True, null=True)
    _link = None

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.sequence.project
        if not self.episode:
            self.episode = self.sequence.episode
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def link(self):
        if not self._link:
            if self.episode:
                self._link = self.episode
            else:
                self._link = self.sequence
        return self._link

    @link.setter
    def link(self, value):
        if value.type == 'Episode':
            self.episode = value
            self.sequence = None
        else:
            self.sequence = value
            self.episode = None
        self.save()
        self._link = None

    @property
    def timecodes(self):
        result = []
        if self.fps and timecode_match(self.timecode_start) and timecode_match(self.timecode_end):
            result = [Timecode(self.fps, self.timecode_start), Timecode(self.fps, self.timecode_end)]
        return result

    @property
    def display_fields(self):
        display_fields =   {'ID': {'value': self.id, 'attribute': 'id'},
                            'Name': {'value': self.long_name, 'attribute': 'long_name'},
                            'Edit type': {'value': self.safe_get('edit_type.long_name'),
                                          '_id': self.safe_get('edit_type.id'),
                                          '_model': self.safe_get('edit_type.type'),
                                          'url': self.safe_get('edit_type.full_url'),
                                          'attribute': 'edit_type'},
                            'Created': {'value': self.date_time_strptime('created'), 'attribute': 'created'},
                            'Created by': {'value': self.created_by, 'attribute': 'created_by'},
                            'Updated': {'value': self.date_time_strptime('updated'), 'attribute': 'updated'},
                            'Updated by': {'value': self.updated_by, 'attribute': 'updated_by'},
                            'Description': {'value': self.description, 'attribute': 'description'},
                            }
        return display_fields


class Attachment(Model, MethodsMixin):
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True, blank=True)
    file = models.FileField(null=True, blank=True, upload_to='attachments/')
    author = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    is_image = models.BooleanField(default=False)
    is_video = models.BooleanField(default=False)
    _parent_model = models.CharField(null=True, max_length=50)
    _parent_id = models.IntegerField(null=True)

    def save(self, *args, **kwargs):
        if not self.project:
            self.project = self.parent.project
        if self.format.lower() in SUPPORTED_IMAGE_EXTENSIONS:
            self.is_image = True
        elif self.format.lower() in SUPPORTED_VIDEO_EXTENSIONS:
            self.is_video = True
        super().save(*args, **kwargs)  # Call the "real" save() method.

    @property
    def name(self):
        return self.file.url.split('/')[-1]

    @property
    def format(self):
        return self.file.url.split('.')[-1]
