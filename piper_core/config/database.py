# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


# A constant dictionary of standard statuses:
STATUS = {
        'active': {'label': 'active', 'applies_to': ['All'], 'is_active': True},
        'removed': {'label': 'removed', 'applies_to': ['All'], 'is_active': False},
        'na': {'label': 'n/a', 'applies_to': ['All'], 'is_active': False},
        'waiting': {'label': 'waiting', 'applies_to': ['Task', 'Asset', 'Episode', 'Shot'], 'is_active': True},
        'ready': {'label': 'ready', 'applies_to': ['Task', 'Asset', 'Episode', 'Shot'], 'is_active': True},
        'in_progress': {'label': 'in progress', 'applies_to': ['Task', 'Asset', 'Episode', 'Sequence', 'Shot', 'Edit'],
                        'is_active': True},
        'rendering': {'label': 'rendering', 'applies_to': ['Version', 'File'], 'is_active': False},
        'complete': {'label': 'complete', 'applies_to': ['Task', 'Episode', 'Shot', 'File'], 'is_active': False},
        'final': {'label': 'final', 'applies_to': ['Task', 'Project', 'Asset', 'Sequence', 'Shot'], 'is_active': True},
        'on_hold': {'label': 'on hold', 'applies_to': ['Task', 'Project', 'Asset', 'Shot', 'Edit'], 'is_active': False},
        'skip': {'label': 'skip', 'applies_to': ['Task', 'Asset', 'Episode', 'Sequence', 'Shot'], 'is_active': False},
        'review': {'label': 'review', 'applies_to': ['Task', 'Version'], 'is_active': True},
        'seen': {'label': 'seen', 'applies_to': ['Version'], 'is_active': True},
        'cbb': {'label': 'could be better', 'applies_to': ['Task', 'Shot'], 'is_active': True},
        'fixing': {'label': 'fixing', 'applies_to': ['Task'], 'is_active': True},
        'approved': {'label': 'approved', 'applies_to': ['Task', 'Edit'], 'is_active': True},
        'archived': {'label': 'archived', 'applies_to': ['Project'], 'is_active': False},
        'bidding': {'label': 'bidding', 'applies_to': ['Project', 'Shot'], 'is_active': True},
        'client_approved': {'label': 'client approved', 'applies_to': ['Asset', 'Shot', 'Version'], 'is_active': True},
        'internally_approved': {'label': 'internally approved', 'applies_to': ['Asset'], 'is_active': True},
        'submit_to_client': {'label': 'submit to client', 'applies_to': ['Shot'], 'is_active': True},
        'submit_to_director': {'label': 'submit to director', 'applies_to': ['Shot'], 'is_active': True}
}

# A constant list of standard categories:
CATEGORIES = ['2d', '3d']

# A constant dictionary of standard pipeline steps:
PIPELINE_STEP = {
    'Shot': {
        'concept': 'cpt',
        'surface': 'srf',
        'modeling': 'mod',
        'digital matte painting': 'dmp',
        'texture': 'txr',
        'rigging': 'rig',
        'animation': 'ani',
        'effects': 'efx',
        'character fx': 'cfx',
        'layout': 'lay',
        'lighting': 'lgt',
        'compositing': 'cmp',
        'slapcomp': 'slp',
        'matchmove': 'mmv',
        'environment': 'env',
        'roto': 'rot',
        'paint': 'pnt',
        'plate': 'plt',
        'camera': 'cam'
    },
    'Asset': {
        'concept': 'cpt',
        'surface': 'srf',
        'modeling': 'mod',
        'digital matte painting': 'dmp',
        'texture': 'txr',
        'rigging': 'rig',
        'animation': 'ani',
        'effects': 'efx',
        'character fx': 'cfx',
        'lighting': 'lgt',
        'lookdev': 'ldv',
        'compositing': 'cmp'
    }
}

# A constant dictionary of pipeline step to category pairs:
PIPELINE_STEP_TO_CATEGORY = {
                                'concept': '2d',
                                'surface': '3d',
                                'modeling': '3d',
                                'digital matte painting': '2d',
                                'texture': '2d',
                                'rigging': '3d',
                                'animation': '3d',
                                'effects': '3d',
                                'character fx': '3d',
                                'layout': '3d',
                                'lighting': '3d',
                                'lookdev': '3d',
                                'compositing': '2d',
                                'slapcomp': '2d',
                                'matchmove': '3d',
                                'environment': '3d',
                                'roto': '2d',
                                'paint': '2d',
                                'plate': '2d',
                                'camera': '3d'
}

# A constant dictionary of default asset types:
ASSET_TYPES = {'character': 'chr',
               'effects': 'efx',
               'environment': 'env',
               'prop': 'prp',
               'vehicle': 'veh'}

# A constant dictionary of default asset types:
EDIT_TYPES = {'storyboards': 'strbrds',
              'animatic': 'anmtc',
              'assembly': 'asbly',
              'client': 'clnt',
              'final': 'fnl'}

# A template of parent-children relationships:
PARENT_TO_CHILDREN = {
            'Software': ['Module'],
            'System': ['Software'],
            'Department': ['User'],
            'Episode': ['Sequence', 'Asset'],
            'Project': ['Episode', 'Sequence', 'Asset'],
            'Sequence': ['Shot'],
            'Asset': ['Task'],
            'Shot': ['Task'],
            'Task': ['Resource', 'WorkLog'],
            'Resource': ['Version'],
            'Version': ['File']
}

# Templates for on-disk path generation:
DEFAULT_PUBLISH_PATH_TEMPLATE = '{project.name}/{episode.name}/{sequence.name}/{link.long_name}/' +\
                                '{resource.category.name}/publish/{resource.long_name}/{version.name}/' +\
                                '{file.format}/{file.name}{file.extension}'

DEFAULT_WORK_PATH_TEMPLATE = '{project.name}/{episode.name}/{sequence.name}/{link.long_name}/' + \
                             '{resource.category.name}/workspace/{resource.created_by}/{resource.long_name}/' +\
                             '{version.name}/{file.format}/{file.name}{file.extension}'

# A template for naming version files:
VERSION_FILE_NAME_TEMPLATE = '{resource.long_name}_{version.name}'
