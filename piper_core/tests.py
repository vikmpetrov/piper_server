# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from django.test import TestCase
from .models import *
from .api import DbApi
from .utils import get_current_user


def populate_entities():
    """
    Create some example entities.
    :return: a list of example entities.
    """
    before = timezone.now()
    status = Status.objects.create(name='active', long_name='active', applies_to='all')
    user = status.created_by
    department = Department.objects.create(name='pipeline',
                                           long_name='pipeline',
                                           status=status,
                                           email='it@example.com')
    user = User.objects.create(name=user, long_name=user, department=department, status=status)
    project = Project.objects.create(name='demo', long_name='demo')
    sequence = Sequence.objects.create(name='xy', long_name='xy', project=project, _parent_id=project.id,
                                       _parent_model=project.type)
    shot = Shot.objects.create(name='0010', long_name='0010', project=project, sequence=sequence, _parent_id=sequence.id,
                               _parent_model=sequence.type)
    asset_type = AssetType.objects.create(name='env', long_name='environment')
    asset = Asset.objects.create(name='jungle', long_name='jungle', project=project, _parent_id=project.id,
                                 _parent_model=project.type, asset_type=asset_type)
    category = Category.objects.create(name='3d', long_name='3d')
    step = Step.objects.create(name='animation', long_name='animation',
                                                level='shot', category=category)
    task = Task.objects.create(name='anim', long_name='animation', shot=shot, project=project, artist=user,
                               step=step, _parent_id=shot.id, _parent_model=shot.type)
    resource = Resource.objects.create(name='walk_cycle', long_name='walk_cycle', artist=user,
                                       source='maya', task=task)
    v0001 = Version.objects.create(artist=user, resource=resource)
    v0001.save()
    v0002 = Version.objects.create(artist=user, resource=resource)
    v0002.save()
    v0003 = Version.objects.create(artist=user, resource=resource)
    v0003.save()

    v0002.comment = 'Example comment.'
    v0002.save()

    dependency = Dependency.objects.create(dependant_id=v0003.id, dependant_model=v0003.type)
    dependency.relatives = [v0001, v0002]
    dependency.save()

    file0001 = File.objects.create(format='ma', artist=user, version=v0001)
    file0002 = File.objects.create(format='mb', artist=user, version=v0001)
    file0001.save()
    file0002.save()

    config = Config.objects.create(name='example_config', long_name='example_config', _parent_model=shot.type,
                                   _parent_id=shot.id)
    config.save()

    # Time sensitive operations:
    work_log = WorkLog.objects.create(name='example_log', long_name='example_log', project=project, artist=user,
                                      task=task, time_from=before, time_to=timezone.now())
    work_log.save()

    task.end_date = timezone.now()
    task.save()

    # Store and return entities:
    entities = {'status': status,
                'department': department,
                'user': user,
                'project': project,
                'sequence': sequence,
                'shot': shot,
                'asset_type': asset_type,
                'asset': asset,
                'step': step,
                'task': task,
                'resource': resource,
                'v0001': v0001,
                'v0002': v0002,
                'v0003': v0003,
                'dependency': dependency,
                'file0001': file0001,
                'file0002': file0002,
                'config': config,
                'work_log': work_log}

    return entities


class ModelsTest(TestCase):
    entities = []

    def setUp(self):
        self.entities = populate_entities()

    def test(self):
        """Models create and query test."""

        # Make sure that all entities exist and can be queried:
        for i in self.entities:
            entity = self.entities[i]
            self.assertIsNotNone(entity)
            entity_model = eval(entity.type)
            query_result = entity_model.objects.get(id=entity.id)
            self.assertIsNotNone(query_result)

        self.assertGreater(self.entities['v0002'].updated, self.entities['v0002'].created)

        # Check parent-child relationship:
        self.assertEqual(self.entities['sequence'].parent, self.entities['project'])
        self.assertIn(self.entities['shot'], self.entities['sequence'].children)

        # Check dependencies:
        self.assertIsNone(self.entities['v0001'].dependencies)
        self.assertIsNotNone(self.entities['v0003'].dependencies)
        dependency = Dependency.objects.get(id=self.entities['dependency'].id)
        self.assertIn(self.entities['v0001'], dependency.relatives)

        # Check resource properties:
        self.assertIsNotNone(self.entities['resource'].category)
        self.assertIsNotNone(self.entities['resource'].link)

        # Check version files:
        self.assertEqual(list(self.entities['v0003'].file_set.all()), [])
        self.assertIsNotNone(self.entities['v0001'].file_set.all())
        self.assertIn(self.entities['file0001'], self.entities['v0001'].file_set.all())

        # Check version properties:
        self.assertTrue(self.entities['v0003'].is_latest)
        self.assertFalse(self.entities['v0002'].is_latest)

        # Check work log:
        work_log = self.entities['work_log']
        self.assertGreater(work_log.time_to, work_log.time_from)
        self.assertIsNotNone(work_log.duration)

        # Check task status:
        self.assertIsNotNone(self.entities['task'].is_active)

        # Check task properties:
        self.assertIsNotNone(self.entities['task'].hours_logged)
        self.assertFalse(self.entities['task'].is_active)

        # Path generation check:
        self.assertIsNone(self.entities['department'].publish_path)
        self.assertIsNone(self.entities['user'].publish_path)
        self.assertIsNone(self.entities['step'].publish_path)
        self.assertIsNone(self.entities['task'].publish_path)
        self.assertIsNone(self.entities['work_log'].publish_path)
        self.assertIsNone(self.entities['asset_type'].publish_path)

        self.assertIsNotNone(self.entities['project'].publish_path)
        self.assertIsNotNone(self.entities['sequence'].publish_path)
        self.assertIsNotNone(self.entities['shot'].publish_path)
        self.assertIsNotNone(self.entities['asset'].publish_path)
        self.assertIsNotNone(self.entities['resource'].publish_path)
        self.assertIsNotNone(self.entities['v0001'].publish_path)
        self.assertIsNotNone(self.entities['file0001'].publish_path)


class DbApiTest(TestCase):
    db_api = None

    def setUp(self):
        self.db_api = DbApi()

    def test(self):
        """Models create and query test."""
        # Make sure the database API methods get expected results:

        pipeline_department, _ = self.db_api.create_entity(entity_type='Department', name='pipeline', locked=True)

        self.db_api.create_entity(entity_type='User',
                                  name=get_current_user(),
                                  department=pipeline_department,
                                  is_administrator=True,
                                  locked=True)

        self.db_api.create_entity(entity_type='Status',
                                  name='active',
                                  long_name='active',
                                  applies_to='All',
                                  is_active=True,
                                  locked=True)

        disabled_status, _ = self.db_api.create_entity(entity_type='Status',
                                                       name='disabled',
                                                       long_name='disabled',
                                                       applies_to='All',
                                                       is_active=False,
                                                       locked=True)

        project, _ = self.db_api.create_entity(entity_type='Project', name='example')
        project_id = project.id
        project_initial = project.jsonify()
        self.assertIsNotNone(project)
        sequence, _ = self.db_api.create_entity(entity_type='Sequence', name='xmp', project=project)
        shot, _ = self.db_api.create_entity(entity_type='Shot', name='0010', sequence=sequence, project=project)
        delete_shot, _ = self.db_api.create_entity(entity_type='Shot', name='0020', sequence=sequence, project=project)
        delete_shot_id = delete_shot.id
        disable_shot, _ = self.db_api.create_entity(entity_type='Shot', name='0030', sequence=sequence, project=project)
        self.db_api.create_entity(entity_type='Shot', name='0040', sequence=sequence, project=project)

        project.description = 'Some description.'
        self.assertNotEqual(first=project_initial['fields'], second=project.jsonify()['fields'])

        self.db_api.delete_entity(delete_shot)
        self.assertNotIn(delete_shot_id, [shot.id for shot in self.db_api.get_entities(entity_type='Shot')])

        self.db_api.disable_entity(disable_shot)
        self.assertEqual(first=disable_shot.status, second=disabled_status)

        config = self.db_api.configure(entity=shot, data={'start_frame': 1001})
        self.assertIsNotNone(config)
        self.assertEqual(config.type, 'Config')

        get_config = self.db_api.get_config(entity=shot)
        self.assertIsNotNone(get_config)
        self.assertEqual(get_config.type, 'Config')

        all_config_data = self.db_api.get_config(entity=shot)
        self.assertIsNotNone(all_config_data)

        self.db_api.update_config(config_entity=config, data={'start_frame': 1002, 'random': True})

        user = self.db_api.current_user
        self.assertIsNotNone(user)
        self.assertEqual(user.type, 'User')

        shots = self.db_api.get_entities(entity_type='Shot')
        self.assertIsNotNone(shots)

        result = self.db_api.get_entity(entity_type='Project', id=project_id)
        self.assertIsNotNone(result)
        self.assertEqual(result.type, 'Project')
