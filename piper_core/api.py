# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
import smtplib
import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from glob import glob
from shutil import copyfile
from django.utils.timezone import make_aware
from django.db.utils import IntegrityError
from django.db.models import Q, QuerySet
from django.conf import settings
from django.template import loader
from .models import *
from .logger import get_logger
from .utils import get_current_user, json_to_string, string_to_json
from .config import SG_SYNCABLE
from .config.database import PARENT_TO_CHILDREN
from piper_dashboard.serializers import MODEL_SERIALIZER

LOGGER = get_logger('Piper Database API')


class DbApi(object):
    """
    An object containing methods to interact with the database.
    """

    _globals = None
    _user = None
    _system = None

    def __init__(self, request=None):
        super(DbApi, self).__init__()
        if request:
            self._user = request.user

    def create_entity(self, **kwargs):
        """
        Create a new entity of a specific type in the database based on the provided keyword arguments.
        """
        entity_str = kwargs.pop('entity_type', None)  # get the entity as a string;
        entity = eval(entity_str)  # get the entity class corresponding to the given entity string;
        LOGGER.info('Creating %s...' % entity_str)

        # Determine the entity's parent if any:
        if '_parent_id' not in kwargs:
            _parent_model = None
            _parent_id = None
            for parent in PARENT_TO_CHILDREN.keys():
                if entity_str in PARENT_TO_CHILDREN[parent] and (parent.lower() in kwargs) and kwargs[parent.lower()]:
                    _parent_model = parent
                    _parent_id = kwargs[parent.lower()].id
                    break
        else:
            _parent_model = kwargs['_parent_model']
            _parent_id = kwargs['_parent_id']

        # Check whether the same entity already exists:
        if entity_str not in ['User', 'Version', 'File', 'Step', 'Software', 'WorkLog', 'Dependency']:
            existing_entities = entity.objects.filter(name=kwargs['name'])
            if _parent_model:
                existing_entities = existing_entities.filter(_parent_id=_parent_id, _parent_model=_parent_model)

            if existing_entities:
                msg = '%s %s already exists, aborting creation...' % (entity_str, kwargs['name'])
                LOGGER.error(msg=msg)
                return False, msg
        elif entity_str == 'Dependency':
            existing_dependency = Dependency.objects.filter(dependant_model=kwargs['dependant_model'],
                                                            dependant_id=kwargs['dependant_id']).first()
            if existing_dependency:
                msg = '%s already exists, aborting creation...' % entity_str
                LOGGER.error(msg=msg)
                return False, msg
        else:
            pass

        # Create the new entity instance and set its attribute values based on the given key/value pairs:
        entity_instance = entity()
        if _parent_model:
            entity_instance._parent_model = _parent_model
            entity_instance._parent_id = _parent_id

        set_after_publish = []
        for key, value in kwargs.items():
            if not isinstance(value, QuerySet):
                setattr(entity_instance, key, value)
            else:
                set_after_publish.append(key)

        # Account for episodic sequences:
        if entity_str == 'Sequence':
            if 'project' not in kwargs:
                entity_instance.project = kwargs['episode'].project

        if hasattr(entity, 'created_by'):
            entity_instance.created_by = self.current_user.username

        # Publish the entity in the database:
        try:
            self.publish(entity_instance)
        except IntegrityError as error:
            msg = '{0} creation error: {1}'.format(entity_str, error)
            LOGGER.error(msg=msg)
            return False, msg

        if set_after_publish:
            for key in set_after_publish:
                value = kwargs[key]
                if isinstance(value, QuerySet):
                    eval('entity_instance.{}.set(value)'.format(key))
            self.publish(entity_instance)

        msg = 'Successfully created %s, unique ID: %s' % (entity_instance, entity_instance.id)
        LOGGER.info(msg=msg)
        return entity_instance, msg

    def _create_entity(self, **kwargs):
        result, _ = self.create_entity(**kwargs)
        return result

    def publish(self, entity):
        """
        Add and commit an entity to the database.
        :param entity: an entity;
        :return: None;
        """
        if hasattr(entity, 'updated_by'):
            entity.updated_by = self.current_user.username
        entity.save()
        self.sg_sync_check(entity=entity)

    @staticmethod
    def delete_entity(entity):
        """
        Delete an entity from the database.
        :param entity: the entity to delete from the database;
        :return: True
        """
        entity.delete()
        return True

    def disable_entity(self, entity):
        """
        Set the status of a given entity to removed.
        :param entity: the entity to remove;
        :return: True
        """
        entity.status = Status.objects.get(name='disabled')
        return self.publish(entity)

    @staticmethod
    def get_config(entity):
        """
        Get the config entity related to a specific entity.
        :param entity: the entity, to which the config entity should relate;
        :return: a config entity;
        """
        return Config.objects.filter(_parent_id=entity.id).first()

    def configure(self, entity, data):
        """
        Create a config entity in the database.
        :param entity: the entity, which the config entity refers to;
        :param data: dictionary data to populate the config entity;
        :return: a newly created config entity;
        """
        config = Config.objects.filter(_parent_model=entity.type, _parent_id=entity.id).first()
        if not config:
            config = Config.objects.create(name=entity.long_name,
                                           _parent_model=entity.type,
                                           _parent_id=entity.id,
                                           config_rules=data)
        else:
            config.config_rules = data
        self.publish(config)
        LOGGER.info('Successfully created configuration %s for %s.' % (config,
                                                                       entity))
        return config

    def update_config(self, config_entity, data):
        """
        Update a config entity with new data.
        :param config_entity: the config entity to update;
        :param data: the data to update the config entity with;
        :return: True
        """
        config_entity.config_rules = data
        self.publish(config_entity)
        LOGGER.info('Successfully updated configuration for %s' % config_entity)
        return True

    @property
    def current_user(self):
        """
        Get the entity for the current user.
        :return: an user entity;
        """
        if not self._user:
            self._user = User.objects.get(name=get_current_user(), status_id=1)
        return self._user

    @staticmethod
    def get_entity(entity_type, id):
        """
        Get an arbitrary database entity given its type and ID.
        :param entity_type: a string representation of an entity class;
        :param id: the entity's unique (ind) ID;
        :return: a single entity matching the given type and ID;
        """
        return eval(entity_type).objects.get(id=id)

    @staticmethod
    def get_entities(entity_type):
        """
        Get all arbitrary database entities.
        :param entity_type: a string representation of an entity class;
        :return: a list of entities;
        """
        entity = eval(entity_type)
        return entity.objects.all()

    @staticmethod
    def filter_entities(entity_type, **kwargs):
        """
        Get filtered database entities.
        :param entity_type: a string representation of an entity class;
        :return: a list of entities;
        """
        entity = eval(entity_type)
        return entity.objects.filter(**kwargs)

    @property
    def globals(self):
        if not self._globals:
            self._globals = GlobalsConfig.objects.get(id=1)
        return self._globals

    @property
    def system(self):
        if not self._system:
            self._system = System.objects.filter(platform=sys.platform).first()
        return self._system

    def register_user(self, username, password, department=None, is_staff=True, is_superuser=False, is_human=True,
                      locked=False, description=None):
        user, _ = self.create_entity(entity_type='User',
                                     username=username,
                                     department=department,
                                     is_staff=is_staff,
                                     is_superuser=is_superuser,
                                     locked=locked,
                                     description=description,
                                     is_human=is_human)
        user.set_password(password)
        user.save()
        return user

    @staticmethod
    def get_entity_activity(entity, index_start=0, index_end=25):
        return entity.activity[index_start:index_end]

    def batch_update_entities(self, request, parent, entity_model, entities_dict_data):
        result = {'warnings': []}
        user = request.user
        for entity_dict in entities_dict_data:
            update = False
            for key, value in entity_dict.items():
                if type(value) == str:
                    entity_dict[key] = value.replace('"', '')
            # Case for empty ID fields:
            if not entity_dict['ID']:
                continue

            # Case for creating new entities:
            elif '+' in str(entity_dict['ID']):
                if not entity_dict['Name']:
                    result['warnings'].append('No name provided for {} creation, skipping.'.format(entity_model))

                create_dict = {'entity_type': entity_model, 'name': entity_dict['Name']}
                if parent:
                    create_dict['_parent_id'] = parent.id
                    create_dict['_parent_model'] = parent.type

                instance, message = self.create_entity(**create_dict)
                if not instance:
                    result['warnings'].append(message)
                    continue

                self.email_notification_check(entity=instance, request=request, action='published')

                if parent:
                    setattr(instance, parent.type.lower(), parent)
                    update = True

            # Case for deleting entities:
            elif '-' in str(entity_dict['ID']):
                entity_id = int(float(str(entity_dict['ID']).replace('-', '')))
                # Ensure the user has permission to delete entities:
                if not user.is_superuser:
                    result['warnings'].append('You are not authorised to delete {0} {1} (ID:{2}), skipping.'.format(
                        entity_model, entity_dict['Name'], entity_id))
                    continue

                try:
                    instance = self.get_entity(entity_type=entity_model, id=entity_id)
                except Exception:
                    result['warnings'].append('{0} {1} (ID: {2}) does not exist, skipping deletion.'.format(
                        entity_model,
                        entity_dict['Name'],
                        entity_id))
                    continue

                if instance.locked:
                    result['warnings'].append('{0} {1} (ID: {2}) is locked and cannot be deleted.'.format(entity_model,
                                                                                                entity_dict['Name'],
                                                                                                instance.id))
                    continue

                self.delete_entity(instance)

                self.email_notification_check(entity=instance, request=request, action='deleted')

            else:
                instance = self.get_entity(entity_type=entity_model, id=int(entity_dict['ID']))

            if instance.locked:
                result['warnings'].append('{0} {1} (ID: {2}) is locked and cannot be modified.'.format(entity_model,
                                                                                             entity_dict['Name'],
                                                                                             instance.id))
                continue

            display_fields = instance.display_fields

            for key, value in entity_dict.items():
                if key == 'Thumbnail':
                    thumbnail = value
                    if not os.path.isfile(thumbnail):
                        result['warnings'].append('{} is not a valid thumbnail file.'.format(thumbnail))
                        continue
                    self.update_thumbnail(entity=instance, thumbnail=thumbnail)

                elif key != 'ID':
                    new_value = value
                    if '_id' in display_fields[key]:
                        entity = eval(display_fields[key]['_model'])
                        query = entity.objects.filter(long_name=value)
                        if not query:
                            query = entity.objects.filter(name=value)

                        if entity.type == 'Step' and parent:
                            query.filter(level=parent.type)

                        if not query:
                            result['warnings'].append('{0} {1} does not exist, skipping.'.format(
                                display_fields[key]['_model'], value))
                            continue

                        new_value = query[0]

                        if new_value.type == 'Status':
                            if new_value.applies_to not in ['All', instance.type]:
                                result['warnings'].append('Status {0} does not apply to {1}s, skipping.'.format(
                                    new_value.long_name, instance.type))
                                continue

                    # Account for date values:
                    if 'date' in key:
                        try:
                            new_value = make_aware(datetime.datetime.strptime(new_value, '%m/%d/%Y'))
                        except (ValueError, TypeError):
                            pass

                    if getattr(instance, display_fields[key]['attribute']) != new_value:
                        try:
                            setattr(instance, display_fields[key]['attribute'], new_value)
                            update = True
                        except TypeError:
                            result['warnings'].append('Could not set the attribute {0} to the value of "{1}".'.format(
                                display_fields[key]['attribute'], new_value))
                            #eval('instance.{0}.set({1})'.format(display_fields[key]['attribute'], new_value))

                        # Ensure renaming is consistent across the entity's "name" and "long_name":
                        if display_fields[key]['attribute'] == 'long_name':
                            instance.name = instance.long_name.lower()
                        if display_fields[key]['attribute'] == 'name':
                            instance.rename()
                else:
                    pass

            if update:
                instance.updated_by = user.username
                instance.save()
                self.email_notification_check(entity=instance, request=request)
                
        return result

    def batch_ingest_entities(self, request, entities_dict_data):
        result = {'warnings': []}
        user = request.user

        def _warning_check(index, name):
            if not name:
                result['warnings'].append('Row {0}: {1} not provided, skipping.'.format(index, name))
                return False
            return True

        for i, entity_dict in enumerate(entities_dict_data):
            index = i+2

            for key, value in entity_dict.items():
                if type(value) == str:
                    entity_dict[key] = value.replace('"', '')

            project_name = entity_dict['Project']
            if not _warning_check(index, project_name):
                continue
            episode_name = entity_dict['Episode']
            sequence_name = entity_dict['Sequence']
            shot_name = entity_dict['Shot']
            asset_name = entity_dict['Asset']
            if not (sequence_name and shot_name) and not asset_name:
                result['warnings'].append('Row {}: No sequence/shot or asset provided, skipping.'.format(index))
                continue

            step_name = entity_dict['Step']
            if not _warning_check(index, step_name):
                continue
            task_name = entity_dict['Task']
            if not _warning_check(index, task_name):
                continue
            resource_name = entity_dict['Resource']
            if not _warning_check(index, resource_name):
                continue
            files = entity_dict['Files']
            if not _warning_check(index, files):
                continue
            files = ' '.join(files.split(','))
            files = files.split()
            description = entity_dict['Description']

            project = Project.objects.filter(Q(name__exact=project_name) |
                                             Q(long_name__exact=project_name)).distinct().first()

            if not project:
                result['warnings'].append('Row {}: unable to find project {}'.format(index, project_name))
                continue

            episode = None
            if episode_name:
                episode = Episode.objects.filter(Q(name__exact=episode_name) |
                                                 Q(long_name__exact=episode_name),
                                                 project=project).distinct().first()

            sequence = None
            if sequence_name:
                sequence = Sequence.objects.filter(Q(name__exact=sequence_name) |
                                                   Q(long_name__exact=sequence_name), episode=episode,
                                                   project=project).distinct().first()

            shot = None
            if shot_name:
                shot = Shot.objects.filter(Q(name__exact=shot_name) |
                                           Q(long_name__exact=shot_name), sequence=sequence).distinct().first()

            asset = None
            if asset_name:
                asset = Asset.objects.filter(Q(name__exact=asset_name) |
                                             Q(long_name__exact=asset_name),
                                             project=project, episode=episode).distinct().first()

            if not asset and not shot:
                result['warnings'].append('Row {}: No valid shot or asset provided...'.format(index))
                continue

            if asset:
                level = 'Asset'
            else:
                level = 'Shot'
            step = Step.objects.filter(Q(name__exact=step_name) |
                                       Q(long_name__exact=step_name), level=level).distinct().first()

            task = Task.objects.filter(Q(name__exact=task_name) |
                                       Q(long_name__exact=task_name),
                                       step=step,
                                       project=project,
                                       asset=asset,
                                       shot=shot).distinct().first()

            resource = Resource.objects.filter(Q(name__exact=resource_name) |
                                               Q(long_name__exact=resource_name), task=task).distinct().first()
            if not resource:
                resource, msg = self.create_entity(entity_type='Resource',
                                                   name=resource_name,
                                                   task=task,
                                                   artist=user,
                                                   description=description,
                                                   source='standalone')
                if not resource:
                    result['warnings'].append('Row {0}: unable to create resource {1}: {2}'.format(index,
                                                                                                   resource_name,
                                                                                                   msg))
                    continue

                self.email_notification_check(entity=resource, request=request, action='published')

            version, msg = self.create_entity(entity_type='Version',
                                              resource=resource,
                                              description=description,
                                              artist=user)

            self.email_notification_check(entity=version, request=request, action='published')

            processed_files = self.process_files(files)

            for file_path in processed_files:
                is_sequence = processed_files[file_path]['is_sequence']
                if not is_sequence and not os.path.isfile(file_path):
                    result['warnings'].append('Row {0}: file {1} not found'.format(index, file_path))
                    continue

                file, msg = self.create_entity(entity_type='File',
                                               version=version,
                                               artist=user,
                                               is_sequence=is_sequence,
                                               format=file_path.split('.')[-1])
                if not file:
                    result['warnings'].append('Row {0}: file not published: {1}'.format(index, msg))
                    continue

                publish_path = os.path.join(self.system.publish_root, file.publish_path)
                publish_dir = os.path.dirname(publish_path)
                if not os.path.isdir(publish_dir):
                    os.makedirs(publish_dir)
                thumbnail = None
                if entity_dict['Thumbnail']:
                    thumbnail = entity_dict['Thumbnail']

                if is_sequence:
                    frame = self.get_start_frame(file_entity=file)
                    elements = processed_files[file_path]['elements']
                    for element in processed_files[file_path]['elements']:
                        copyfile(element, publish_path.replace('.####.', '.{}.'.format(frame)))
                        frame += 1
                    if not thumbnail and elements:
                        thumbnail = elements[len(elements)/2]
                else:
                    copyfile(file_path, publish_path)
                    if not thumbnail:
                        thumbnail = publish_path

                if thumbnail:
                    if os.path.isfile(thumbnail):
                        self.update_thumbnail(entity=file, thumbnail=thumbnail)
                    else:
                        result['warnings'].append('{} is not a valid thumbnail file.'.format(thumbnail))

        return result

    @staticmethod
    def update_thumbnail(entity, thumbnail):
        thumbnail_path = 'thumbnails/' + os.path.basename(thumbnail)
        copyfile(thumbnail, os.path.join(settings.MEDIA_ROOT, thumbnail_path))
        entity.thumbnail = thumbnail_path
        entity.save()
        entity.user_set_thumbnail = True
        entity.save()

    def get_start_frame(self, file_entity):
        if file_entity.is_sequence:
            config = self.get_config(entity=file_entity.version.resource)
            if config:
                start_frame = 1001
                for i in config.inherited_config_rules:
                    if 'start_frame' in config.inherited_config_rules[i]['data']:
                        start_frame = config.inherited_config_rules[i]['data']['start_frame']
                return start_frame
            return self.globals.start_frame
        return None

    @staticmethod
    def process_files(files):
        processed_files = {}
        for file in files:
            file_components = os.path.basename(file).split('.')
            if len(file_components) > 2:
                file_index = file_components[-2]
                if file_index == '*':
                    if file not in processed_files:
                        processed_files[file] = {'is_sequence': True, 'elements': glob(file)}
                elif file_index.startswith('#') or file_index[0].isdigit():
                    file = file.replace('.{}.'.format(file_index), '.*.')
                    if file not in processed_files:
                        processed_files[file] = {'is_sequence': True, 'elements': glob(file)}
                else:
                    processed_files[file] = {'is_sequence': False}
            else:
                processed_files[file] = {'is_sequence': False}
        return processed_files

    @staticmethod
    def serialize(entity, jsonify=True):
        result = MODEL_SERIALIZER[entity.type](entity)
        if jsonify:
            result = result.data
        return result

    def project_tree(self, project, serialized=False):
        tree = {'project': self.serialize(project) if serialized else project,
                'assets': {}, 'episodes': {}, 'sequences': {}}

        for asset in project.asset_set.all():
            tree['assets'][asset.long_name] = {'entity': self.serialize(asset) if serialized else asset, 'children': {}}

            for resource in asset.resources:
                tree['assets'][asset.long_name]['children'][resource.long_name] = {
                    'entity': self.serialize(resource) if serialized else resource, 'children': {}
                }

                for version in resource.version_set.all():
                    tree['assets'][asset.long_name]['children'][resource.long_name]['children'][version.long_name] = {
                        'entity': self.serialize(version) if serialized else version, 'children': {}
                    }

        for episode in project.episode_set.all():
            tree['episodes'][episode.long_name] = {'entity': self.serialize(episode) if serialized else episode,
                                                   'children': self.sequence_tree(episode)}

        tree['sequences'] = self.sequence_tree(project, serialized)

        return tree

    def sequence_tree(self, parent, serialized=True):
        tree = {}
        for sequence in parent.sequence_set.all():
            tree[sequence.long_name] = {'entity': self.serialize(sequence) if serialized else sequence, 'children': {}}

            for shot in sequence.shot_set.all():
                tree[sequence.long_name]['children'][shot.long_name] = {
                    'entity': self.serialize(shot) if serialized else shot,
                    'children': {}}

                for resource in shot.resources:
                    tree[sequence.long_name]['children'][shot.long_name]['children'][
                        resource.long_name] = {'entity': self.serialize(resource) if serialized else resource,
                                               'children': {}}

                    for version in resource.version_set.all():
                        tree[sequence.long_name]['children'][shot.long_name]['children'][
                            resource.long_name]['children'][version.long_name] = {
                            'entity': self.serialize(version) if serialized else version,
                            'children': {}}
        return tree

    def notify(self, target, subject, message, from_email=None, verbose_email=False):
        warnings = []
        if self.globals.email_ready:
            if type(target) == list:
                elements = target
            else:
                elements = [x for x in ' '.join(target.split(',')).split() if x]
            recipients = []
            for element in elements:
                if verbose_email:
                    recipients.append(element)
                else:
                    username = element.replace('@', '')
                    notify_user = User.objects.filter(username=username).first()
                    if notify_user and notify_user.email:
                        recipients.append(notify_user.email)
                    else:
                        warnings.append('Invalid e-mail recipient: {}'.format(username))

            if not from_email:
                from_email = self.globals.email_username
            if recipients:
                try:
                    msg = MIMEMultipart('alternative')
                    msg['From'] = from_email
                    msg['To'] = ', '.join(recipients)
                    msg['Subject'] = subject
                    plain = MIMEText(message, 'plain')
                    html = MIMEText(message, 'html')
                    msg.attach(plain)
                    msg.attach(html)
                    server = smtplib.SMTP(self.globals.email_host, self.globals.email_port)
                    server.starttls()
                    server.login(self.globals.email_username, self.globals.email_password)
                    server.sendmail(from_email, recipients, msg.as_string())
                    server.quit()
                except smtplib.SMTPException as error:
                    warnings.append(error)
            else:
                warnings.append('No valid e-mail recipients found...')
        else:
            warnings.append('Piper is not configured to send e-mail yet...')
        return warnings

    def email_notification_check(self, entity, request, action='updated'):
        if self.globals.email_on_update and self.globals.email_ready:
            recipients = [user.email for user in entity.project.user_set.all() if user.email]
            user = request.user
            if hasattr(request, 'data'):
                if 'updated_by' in request.data:
                    user = User.objects.filter(username=request.data['updated_by']).first()
                elif 'created_by' in request.data:
                    user = User.objects.filter(username=request.data['created_by']).first()

            history = None
            if action == 'updated' and entity.history:
                history = entity.history[0]
            subject = 'Piper | {0} | {1} {2} {3}'.format(entity.project.long_name,
                                                         action.title(),
                                                         entity.type,
                                                         entity.long_name)
            message = loader.render_to_string('email.html',
                                              context={'entity': entity, 'action': action, 'history': history,
                                                       'request': request, 'user': user, 'subject': subject})
            return self.notify(target=recipients,
                               subject=subject,
                               message=message,
                               verbose_email=True)

    def sg_sync_check(self, entity, delete=False):
        if self.globals.can_sg_sync:
            return self.sync_to_sg(entity=entity, delete=delete)

    def sync_to_sg(self, entity, delete=False):
        try:
            if entity.type in SG_SYNCABLE:
                import shotgun_api3
                sg = shotgun_api3.Shotgun(self.globals.shotgun_url,
                                          self.globals.shotgun_script_name,
                                          self.globals.shotgun_application_key)

                if entity.type != 'Project' and not entity.project.sg_id:
                    msg = 'Entity project {} needs to be synced to Shotgun first.'.format(entity.project.long_name)
                    LOGGER.warn(msg)
                    return False, msg

                if not entity.sg_id and not delete:
                    create_data = {
                        'code': entity.project.long_name,
                    }
                    if entity.type != 'Project':
                        create_data['project'] = {'type': 'Project', 'id': entity.project.sg_id}
                    sg_entity = sg.create(entity.type, create_data)
                    entity.sg_id = sg_entity['id']
                    entity.save()

                if delte:
                    LOGGER.info('Deleting {0} {1} on Shotgun.'.format(entity.type, entity.sg_id))
                    return True, sg.delete(entity.type, entity.sg_id)

                update_data = {
                    'description': entity.description,
                    'code': entity.long_name
                }

                if entity.type == 'Version':
                    sg_user = sg.find_one('HumanUser', [['login', 'is', entity.created_by]])
                    if sg_user:
                        update_data['user'] = sg_user

                    if entity.task.sg_id:
                        sg_task = sg.find_one('Task', [['id', 'is', entity.task.sg_id]])
                        update_data['sg_task'] = sg_task

                    if entity.task.link.sg_id:
                        link = sg.find_one(entity.task.link.type, [['id', 'is', entity.task.link.sg_id]])
                        update_data['entity'] = link

                    if entity.preview:
                        update_data['sg_path_to_movie'] = entity.preview.publish_path

                    update_data['sg_version_type'] = 'Publish'
                    if entity.for_dailies:
                        update_data['sg_version_type'] = 'Dailies'

                    update_data['sg_published_path'] = entity.publish_path

                elif entity.type == 'Task':
                    if entity.task.link.sg_id:
                        link = sg.find_one(entity.task.link.type, [['id', 'is', entity.task.link.sg_id]])
                        update_data['entity'] = link

                    if entity.artist.sg_id:
                        sg_artist = sg.find_one('HumanUser', [['id', 'is', entity.artist.sg_id]])
                        if artist:
                            update_data['task_assignees'] = [sg_artist]

                    if entity.entity.episode and entity.episode.sg_id:
                        sg_episode = sg.find_one('Episode', [['id', 'is', entity.episode.sg_id]])
                        if sg_episode:
                            update_data['sg_episode'] = sg_episode

                    update_data['start_date'] = entity.start_date.strftime('%m/%d/%Y')
                    update_data['end_date'] = entity.end_date.strftime('%m/%d/%Y')

                elif entity.type == 'Asset':
                    if entity.episode and entity.episode.sg_id:
                        sg_episode = sg.find_one('Episode', [['id', 'is', entity.episode.sg_id]])
                        if sg_episode:
                            update_data['episodes'] = [sg_episode]
                    update_data['sg_asset_type'] = entity.asset_type.name

                elif entity.type == 'Shot':
                    if entity.entity.episode and entity.episode.sg_id:
                        sg_episode = sg.find_one('Episode', [['id', 'is', entity.episode.sg_id]])
                        if sg_episode:
                            update_data['sg_episode'] = sg_episode

                    if entity.entity.sequence and entity.sequence.sg_id:
                        sg_sequence = sg.find_one('Sequence', [['id', 'is', entity.sequence.sg_id]])
                        if sg_sequence:
                            update_data['sg_sequence'] = sg_sequence

                elif entity.type == 'Sequence':
                    if entity.entity.episode and entity.episode.sg_id:
                        sg_episode = sg.find_one('Episode', [['id', 'is', entity.episode.sg_id]])
                        if sg_episode:
                            update_data['sg_episode'] = sg_episode

                else:
                    pass

                sg.upload_thumbnail(entity.type, entity.sg_id, os.path.join(settings.BASE_DIR, entity.thumbnail_url))

                sg_entity = sg.update(entity.type, entity.sg_id, update_data)
                LOGGER.info('Shotgun sync complete: %s' % sg_entity)
                return True, sg_entity

            msg = '{}s are not configured to sync to Shotgun.'.format(entity.type)
            LOGGER.warn(msg)
            return False, msg

        except Exception as error:
            msg = 'Shotgun sync attempt failed: %s'.format(error)
            LOGGER.error(msg)
            return False, msg
