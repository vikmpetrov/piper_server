# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import os
import sys
from psycopg2 import OperationalError as psycopg2_error
from django.conf import settings
from django.apps import AppConfig
from django.db.utils import OperationalError
from django.core.exceptions import AppRegistryNotReady
from django.db.utils import ProgrammingError
from .utils import read_json, get_current_user
from .logger import get_logger
from .config import APACHE_CONF, SUPPORTED_IMAGE_EXTENSIONS

LOGGER = get_logger(name='Piper Core Config')


class DummyUser:
    username = get_current_user()


class PiperCoreConfig(AppConfig):
    name = 'piper_core'
    verbose_name = "Piper Core"
    db_api = None

    def ready(self):
        try:
            from .api import DbApi
            self.db_api = DbApi()
            self.db_api._user = DummyUser()
        except (AppRegistryNotReady, ProgrammingError, psycopg2_error):
            LOGGER.warn('Initial database population was unsuccessful.')
            return LOGGER.error('App configuration - Failed.')
        try:
            if not self.db_api.get_entities('GlobalsConfig'):
                studio_name = None
                while not studio_name:
                    studio_name = input('Please enter studio name:')
                self.db_api.create_entity(entity_type='GlobalsConfig', name='globals', studio_name=studio_name)

            if not self.db_api.get_entities('Category'):
                from .config.database import CATEGORIES
                for category in CATEGORIES:
                    category_entity, _ = self.db_api.create_entity(entity_type='Category',
                                                                   name=category,
                                                                   long_name=category,
                                                                   locked=True)
                    LOGGER.debug('Created default category - %s.' % category_entity.long_name)

            category_2d = self.db_api.get_entity(entity_type='Category', id=1)
            category_3d = self.db_api.get_entity(entity_type='Category', id=2)
            categories = {'2d': category_2d, '3d': category_3d, '': None}

            maya_module = self.db_api.filter_entities(entity_type='Module', name='maya_piper').first()
            if not maya_module:
                maya_module, _ = self.db_api.create_entity(entity_type='Module', name='maya_piper',
                                                           root='/studio/modules/maya/piper', locked=True)

            nuke_module = self.db_api.filter_entities(entity_type='Module', name='nuke_piper').first()
            if not nuke_module:
                nuke_module, _ = self.db_api.create_entity(entity_type='Module', name='nuke_piper',
                                                           root='/studio/modules/nuke/piper', locked=True)

            houdini_module = self.db_api.filter_entities(entity_type='Module', name='houdini_piper').first()
            if not houdini_module:
                houdini_module, _ = self.db_api.create_entity(entity_type='Module', name='houdini_piper',
                                                              root='/studio/modules/houdini/piper', locked=True)

            if not self.db_api.get_entities('Profile'):
                studio_profile, _ = self.db_api.create_entity(entity_type='Profile', name='studio', locked=True)
                studio_profile.modules.add(maya_module)
                studio_profile.modules.add(nuke_module)
                studio_profile.modules.add(houdini_module)
                studio_profile.save()
            else:
                studio_profile = self.db_api.get_entity(entity_type='Profile', id=1)

            if not self.db_api.get_entities('System'):
                default_studio_config = read_json(os.path.abspath(os.path.join(
                    os.path.dirname(__file__), 'config', 'default_studio_config.json')))

                for platform in default_studio_config:
                    config = default_studio_config[platform]['fields']
                    system, _ = self.db_api.create_entity(entity_type='System',
                                                          platform=platform,
                                                          work_root=config['work_root'],
                                                          publish_root=config['publish_root'],
                                                          name=default_studio_config[platform][
                                                              'long_name'].lower(),
                                                          modules_root=config['modules_root'],
                                                          software_root=config['software_root'],
                                                          long_name=default_studio_config[platform]['long_name'])
                    LOGGER.debug('Created default system configuration for the %s platform.' % system.platform)

                    for software in default_studio_config[platform]['software']:
                        config = default_studio_config[platform]['software'][software]
                        if 'render_formats' in config:
                            render_formats = config['render_formats']
                        else:
                            render_formats = SUPPORTED_IMAGE_EXTENSIONS
                        software, _ = self.db_api.create_entity(entity_type='Software',
                                                                category=categories[config['category']],
                                                                name=software,
                                                                executable=config['executable'],
                                                                file_extensions=config['file_extensions'],
                                                                command_line_renderer=config['command_line_renderer'],
                                                                is_farm=config['is_farm'],
                                                                render_formats=render_formats,
                                                                module_environment_variable=config[
                                                                    'module_environment_variable'],
                                                                locked=True,
                                                                system=system)
                        LOGGER.debug('Created default software configuration for %s on %s.'
                                     % (software.name, system.platform))

                        if not software.is_farm:
                            studio_profile.software.add(software)
                            studio_profile.save()

                        if software.name.startswith('maya'):
                            module = maya_module
                        elif software.name.startswith('nuke'):
                            module = nuke_module
                        elif software.name.startswith('houdini'):
                            module = houdini_module
                        else:
                            continue

                        module.software.add(software)
                        module.save()

            if not self.db_api.get_entities('Status'):
                from .config.database import STATUS
                for status_name in STATUS:
                    status_entity, _ = self.db_api.create_entity(entity_type='Status',
                                                                 name=status_name,
                                                                 long_name=STATUS[status_name]['label'],
                                                                 applies_to=STATUS[status_name]['applies_to'],
                                                                 is_active=STATUS[status_name]['is_active'],
                                                                 locked=True)
                    LOGGER.debug('Created default status %s for entities of type %s.' % (status_entity.long_name,
                                                                                         ','.join(STATUS[status_name
                                                                                                  ]['applies_to'])))

            if not self.db_api.get_entities('Department'):
                pipeline_department, _ = self.db_api.create_entity(entity_type='Department', name='pipeline',
                                                                   locked=True)
                LOGGER.debug('Created default department - %s.' % pipeline_department.long_name)

                it_department, _ = self.db_api.create_entity(entity_type='Department', name='it', long_name='it',
                                                             locked=True)
                LOGGER.debug('Created default department %s.' % it_department.long_name)

            if not self.db_api.get_entities('User'):
                it_department = self.db_api.get_entity(entity_type='Department', id=1)
                password = None
                while not password:
                    password = input('Please enter a password for User administrator:')
                admin = self.db_api.register_user(username='administrator',
                                                  department=it_department,
                                                  is_staff=True,
                                                  is_superuser=True,
                                                  locked=True,
                                                  password=password)
                LOGGER.debug('Created default user - %s.' % admin.long_name)

                current_user = get_current_user()
                pipeline_department = self.db_api.get_entity(entity_type='Department', id=1)
                password = None
                while not password:
                    password = input('Please enter a password for User {}:'.format(current_user))
                current_user_entity = self.db_api.register_user(username=current_user,
                                                                department=pipeline_department,
                                                                is_staff=True,
                                                                is_superuser=True,
                                                                locked=True,
                                                                password=password)

                LOGGER.debug('Created default user %s.' % current_user_entity.long_name)

                robot = self.db_api.register_user(username='piper_client_api',
                                                  department=pipeline_department,
                                                  is_staff=True,
                                                  is_superuser=False,
                                                  password='piper_client_pass',
                                                  locked=True,
                                                  description='Piper Client API robot user',
                                                  is_human=False)

                LOGGER.debug('Created default robot user - %s.' % robot.long_name)

            if not self.db_api.get_entities('AssetType'):
                from .config.database import ASSET_TYPES
                for asset_type in ASSET_TYPES:
                    asset_type_entity, _ = self.db_api.create_entity(entity_type='AssetType',
                                                                     name=ASSET_TYPES[asset_type],
                                                                     long_name=asset_type,
                                                                     locked=True)
                    LOGGER.debug('Created default asset type - %s.' % asset_type_entity.long_name)

            if not self.db_api.get_entities('Step'):

                from .config.database import PIPELINE_STEP, PIPELINE_STEP_TO_CATEGORY
                for level in PIPELINE_STEP:
                    for step in PIPELINE_STEP[level]:
                        step_entity, _ = self.db_api.create_entity(entity_type='Step',
                                                                   level=level,
                                                                   name=PIPELINE_STEP[level][step],
                                                                   long_name=step,
                                                                   category=categories[
                                                                       PIPELINE_STEP_TO_CATEGORY[
                                                                       step]],
                                                                   locked=True)
                        LOGGER.debug('Created default pipeline step - %s.' % step_entity.long_name)

            if not self.db_api.get_entities('EditType'):
                from .config.database import EDIT_TYPES
                for edit_type in EDIT_TYPES:
                    edit_type_entity, _ = self.db_api.create_entity(entity_type='EditType',
                                                                    name=EDIT_TYPES[edit_type],
                                                                    long_name=edit_type,
                                                                    locked=True)
                    LOGGER.debug('Created default edit type - %s.' % edit_type_entity.long_name)

            if not self.db_api.get_entities('Project'):
                from .initialize import initialize_demo_projects
                initialize_demo_projects()
                LOGGER.debug('Set up demo projects.')

        except (OperationalError, ProgrammingError, psycopg2_error):
            LOGGER.warn('Initial database population was unsuccessful.')
            return LOGGER.error('App configuration - Failed.')

        # Export Apache config:
        if not settings.DEBUG:
            venv_root = os.path.join(settings.BASE_DIR, 'venv')

            if sys.platform == 'win32':
                mod_extension = '.pyd'
                site_packages = os.path.join(venv_root, 'Lib', 'site-packages')
            else:
                mod_extension = '.so'
                site_packages = os.path.join(venv_root,
                                             'lib',
                                             '{}'.format([x for x in os.listdir(
                                                 os.path.join(venv_root, 'lib'))
                                                          if not x.startswith('.')][0]),
                                             'site-packages')

            mod_wsgi_root = os.path.join(site_packages, 'mod_wsgi', 'server')
            mod_wsgi = os.path.join(mod_wsgi_root, [x for x in os.listdir(mod_wsgi_root)
                                                    if x.endswith(mod_extension)][0])

            system = self.db_api.filter_entities(entity_type='System', platform=sys.platform)[0]
            server_root = os.path.join(settings.BASE_DIR, 'resources', sys.platform, 'Apache24')
            apache_config = APACHE_CONF.format(
                SERVER_ROOT=server_root,
                MEDIA_ROOT=settings.MEDIA_ROOT,
                STATIC_ROOT=settings.STATIC_ROOT,
                DATA_ROOT=system.publish_root,
                WSGI_ROOT=os.path.join(settings.BASE_DIR, 'piper_server_app'),
                WSGI_FILE=os.path.join(settings.BASE_DIR, 'piper_server_app', 'wsgi.py'),
                VENV_ROOT=venv_root,
                BASE_DIR=os.path.join(settings.BASE_DIR),
                MOD_WSGI=mod_wsgi,
                PORT=settings.PIPER_SERVER_PORT
            )
            apache_conf_dir = os.path.join(server_root, 'conf', 'httpd.conf')
            with open(apache_conf_dir, 'w+') as conf_file:
                conf_file.write(apache_config)

        LOGGER.debug('App instance configuration - OK.')
        return LOGGER.debug('App instance running...')
