echo Archiving Piper Server...
@echo off
call %~dp0venv\Scripts\activate.bat
python manage.py archive
echo Piper Server has been archived.