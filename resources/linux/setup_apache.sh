#!/bin/bash
echo Setting up Apache...
curl --remote-name http://piperpipeline.com/resources/src/httpd-2.4.38.tar.gz
tar -xzvf httpd-2.4.38.tar.gz

curl --remote-name http://piperpipeline.com/resources/src/apr-1.6.5.tar.gz
mkdir -p ./httpd-2.4.38/srclib/apr
tar -xzvf apr-1.6.5.tar.gz --strip 1 --directory ./httpd-2.4.38/srclib/apr

curl --remote-name http://piperpipeline.com/resources/src/apr-util-1.6.1.tar.gz
mkdir -p ./httpd-2.4.38/srclib/apr-util
tar -xzvf apr-util-1.6.1.tar.gz --strip 1 --directory ./httpd-2.4.38/srclib/apr-util

cd ./httpd-2.4.38
./configure --prefix=$PIPER/resources/$PIPER_OS/Apache24 --enable-so --with-included-apr
make
make install
cd ..
echo Apache has been set up.