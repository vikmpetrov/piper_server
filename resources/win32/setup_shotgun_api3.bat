echo Setting up Shotgun API3...
powershell -Command "Invoke-WebRequest www.piperpipeline.com/resources/src/shotgun_api3.zip -OutFile shotgun_api3.zip"
powershell Expand-Archive shotgun_api3.zip -DestinationPath . -Force
echo Shotgun API3 has been set up.