# Piper Server
Piper Server is the web application component of the Piper Pipeline Toolkit - an out-of-the-box CGI production tracking 
and data management software suite.

Developed and maintained by [Viktor Petrov](https://gitlab.com/vikmpetrov), Piper I/O EOOD.

## Version
1.0.1

## Installation & configuration
To install this software, simply place this package anywhere you deem appropriate on your server.

### 1. Setting up a virtual environment
Prerequisite: download and install [Python](https://www.python.org/) (version 3.6 or above)

### On Windows:
Navigate to this directory and run the following script:
```
.\setup.bat
```
Or just double-click on the file run the script.

### On Linux:
Open a terminal shell, navigate to this directory and run the following script:
```
source setup.sh
```

### On Mac:
Open a terminal shell, navigate to this directory and run the following script:
```
source setup.command
```

### 2. Setting up a PostgreSQL database
Prerequisite: download and install [PostgreSQL](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads).

Note that some installations will require you to set a server-wide PostgreSQL password, which you should remember for later use.

#### On Windows:
(Optional) Save your PostgreSQL password in your pgpass.conf file, e.g. "C:\Users\username\AppData\Roaming\postgresql\pgpass.conf":

```
localhost:5432:*:postgres:password
127.0.0.1:5432:*:postgres:password
```

Find and execute the SQL Shell (psql), press enter to all the default settings and enter your password until you get to a "postgres=#" shell and run the following commands:

```
CREATE DATABASE piper_db;
CREATE USER piper_user WITH PASSWORD 'piper_pass';
ALTER ROLE piper_user SET client_encoding TO 'utf8';
ALTER ROLE piper_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE piper_user SET timezone TO 'UTC';
ALTER USER piper_user CREATEDB;
GRANT ALL PRIVILEGES ON DATABASE piper_db TO piper_user;
\q
```

#### On Linux:
Open a terminal shell and run the following commands:
```
sudo -su postgres
psql
CREATE DATABASE piper_db;
CREATE USER piper_user WITH PASSWORD 'piper_pass';
ALTER ROLE piper_user SET client_encoding TO 'utf8';
ALTER ROLE piper_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE piper_user SET timezone TO 'UTC';
ALTER USER piper_user CREATEDB;
GRANT ALL PRIVILEGES ON DATABASE piper_db TO piper_user;
\q
exit
```

#### On Mac:
Open a terminal shell and run the following commands:
```
pg_ctl -D /usr/local/var/postgres start && brew services start postgresql
psql postgres
CREATE DATABASE piper_db;
CREATE USER piper_user WITH PASSWORD 'piper_pass';
ALTER ROLE piper_user SET client_encoding TO 'utf8';
ALTER ROLE piper_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE piper_user SET timezone TO 'UTC';
ALTER USER piper_user CREATEDB;
GRANT ALL PRIVILEGES ON DATABASE piper_db TO piper_user;
\q
```

## Deployment

### On Windows:
Open a Windows Power shell, navigate to this directory and run the following command:
```
.\run.bat
```

### On Linux:
Open a terminal shell, navigate to this directory and run the following command:
```
source run.sh
```

### On Mac:
Open a terminal shell, navigate to this directory and run the following command:
```
source run.command
```

## License

This software package's EULA is outlined in its [LICENSE.md](LICENSE.md) file.

## Copyright
&copy; Piper I/O EOOD. All rights reserved.
