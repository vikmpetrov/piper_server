echo Setting up Piper Server, please wait...
export PIPER_SERVER_ROOT=$PWD
export PIPER_OS=darwin
echo Setting up XCode tools...
xcode-select --install
cd ./resources/darwin
source setup_python.command
source setup_apache.command
source setup_shotgun_api3.command
cd $PIPER_SERVER_ROOT
echo Setting up virtual environment...
$PIPER_SERVER_ROOT/resources/darwin/Python37/bin/python3 -m pip install virtualenv
$PIPER_SERVER_ROOT/resources/darwin/Python37/bin/python3 -m virtualenv venv -p $PIPER_SERVER_ROOT/resources/darwin/Python37/bin/python3
source venv/bin/activate
pip install -r requirements.txt
echo Piper Server setup completed.